import sys
import os
query_store_path = os.path.join(os.path.dirname(__file__), "./queryKibana/")
Anomly_Score_path = os.path.join(os.path.dirname(__file__), "./AnomalyDetection/")
alarms_lark_path = os.path.join(os.path.dirname(__file__), "./FetchAnomly_SavePNG/alarms_to_lark")
write_to_tos_path = os.path.join(os.path.dirname(__file__), "./FetchAnomly_SavePNG/")
sys.path.append(query_store_path)
from query_store import *
sys.path.append(Anomly_Score_path)
from Anomly_Score import *
sys.path.append(alarms_lark_path)
from alarms_lark import alarm_image_text_lark
sys.path.append(write_to_tos_path)
from fetch_true_series import write_to_tos

import datetime

# User Customize:
FROM_DATE = datetime.date(2018,6,26)
TO_DATE = datetime.date(2018,6,26)

# daterange: inclue FROM_DATE and TO_DATE
def daterange(FROM_DATE, TO_DATE):
    for n in range(int((TO_DATE - FROM_DATE).days) + 1):
        yield FROM_DATE + datetime.timedelta(n)

# RUN THE LOOP:
for single_date in daterange(FROM_DATE, TO_DATE):
    DATE = single_date.strftime("%Y-%m-%d")
    print("DATE, ", DATE)
    # Update 5 necessary variable from ads facebook index
    PLATFORM = fetchData('ad_platform', DATE)
    APPID = fetchData('app_id',DATE)
    CAMPAIGNTAG = fetchData('campaign_tag',DATE)
    REGIONS = fetchData('region',DATE)
    APPSERIES = ["NOT (app_id: 1145 OR app_id: 1180 OR app_id: 1233)","app_id: 1145 OR app_id: 1180 OR app_id: 1233"]
    # Update 5 dimensions info from monitor index
    PLATFORM_monitor = fetchData_monitor('ad_platform', TO_DATE)
    APPID_monitor = fetchData_monitor('app_id', TO_DATE)
    CAMPAIGNTAG_monitor = fetchData_monitor('campaign_tag', TO_DATE)
    REGIONS_monitor = fetchData_monitor('region', TO_DATE)
    APPSERIES_monitor = fetchData_monitor('app_series', TO_DATE)
    if DEBUG:
        print("=" * 80)
        print("Platform = ", PLATFORM)
        print("=" * 80)
        print("app id = ", APPID)
        print(len(APPID))
        print("=" * 80)
        print("app series = ", APPSERIES)
        print("=" * 80)
        print("campaign tag = ", CAMPAIGNTAG)
        print("=" * 80)
        print("region = ", REGIONS)
        print(len(REGIONS))
    try:
        #index_by_day(DATE, PLATFORM, APPID, APPSERIES, CAMPAIGNTAG, REGIONS)
        ##score_by_day(DATE, PLATFORM_monitor, APPID_monitor, APPSERIES_monitor, CAMPAIGNTAG_monitor, REGIONS_monitor)
        keylist, objlist = write_to_tos(TO_DATE)
        response = alarm_image_text_lark(keylist, objlist)
        if response:
            print("Success")
        else:
            print("Failed")

    except Exception as err:
        raise Exception(err)

