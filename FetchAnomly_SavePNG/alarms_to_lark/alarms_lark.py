# -*- coding: utf-8 -*
import requests
import os
import yaml
import datetime
import sys
import json
fetch_true_series_path = os.path.join(os.path.dirname(__file__), "./../")
sys.path.append(fetch_true_series_path)
from fetch_true_series import write_to_tos

########################################################################################################################
###################################################### Settings ########################################################
########################################################################################################################
# Setting configuration
config = os.path.join(os.path.dirname(__file__), "config.yaml")
with open(config, "r") as f:
    settings = yaml.load(f)
REQUESTURL = settings['REQUESTURL']
BOT_TOKEN = settings['BOT_TOKEN']
HEADERS = {
    'Content-Type': 'application/json;charset=UTF-8'
}
today = datetime.datetime.today().strftime("%Y-%m-%d")
title = today + "请点击消息查看facebook ads 异常警报"

########################################################################################################################
###################################################### LarkAlarm #######################################################
########################################################################################################################
def alarm_url_to_lark(key_list):
    text = ",\n".join(key_list)
    data = {
              "token": BOT_TOKEN,
              "email": "huangyingying.doro@bytedance.com, huangxin@bytedance.com",
              "msg_type": "post",
              "content": {
                "text": text,
                "title": title
              }
            }
    data = json.dumps(data)
    response = requests.post(REQUESTURL, headers=HEADERS, files=image_dict, data=data)
    return response

def alarm_image_text_lark(key_list, obj_list):
    try:
        for image_url,obj in zip(key_list, obj_list):
            data = {
                    "token": BOT_TOKEN,
                    #"email": "huangyingying.doro@bytedance.com",
                    "msg_type": "post",
                    "chat_id":"6574965963093967118",
                    "content": {
                        "text": "<p>异常情况报警</p><p>platform: "+str(obj["ad_platform"])+" series: "+str(obj["app_series"])+" id: "+str(obj["app_id"])+" campaign_tag: "+str(obj["campaign_tag"])+" region: "+str(obj["region"])+" metric_type: "+str(obj["metric_type"])+"</p><p>image_irl: "+str(image_url)+"</p><figure><img src=\""+str(image_url)+"\" origin-width=\"1280\" origin-height=\"640\"/></figure>",
                        "title": title
                        }
                    }
            data = json.dumps(data)
            response = requests.post(REQUESTURL, headers=HEADERS, data=data)
            print(response.json())
        return True
    except Exception as err:
        raise Exception(err)
        
###################################################### MainFunc ########################################################
########################################################################################################################
#keylist, objlist = write_to_tos(datetime.date(2018,6,23))
#response = alarm_image_text_lark(keylist, objlist)
#print(response)
