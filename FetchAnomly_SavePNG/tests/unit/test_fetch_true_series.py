import unittest
import os
import sys
fetch_true_series_path = os.path.join(os.path.dirname(__file__), "./../../")
sys.path.append(fetch_true_series_path)
from fetch_true_series import *
from unittest import mock
import numpy as np
import datetime
import elasticsearch




class MyTestCase(unittest.TestCase):
    def setUp(self):
        self.obj = {
                                'ad_platform': "facebook",
                                'app_series': "IES",
                                'app_id': "1180",
                                'region': "US",
                                'campaign_tag': "NULL",
                                'metric_type': "installs"
                            }
        self.FROM_DATE = datetime.date(2018,5,31)
        self.TO_DATE = datetime.date(2018,6,13)
        self.THREAD_COUNT = 6
        self.QUEUE_SIZE = 6
        self.WINDOW_SIZE = 6
        self.PAA_SEGMENTS = 5
        self.SAX_WORDS = 3
        self.KNN_SIZE = 3
        self.SERIES_LENGTH = 14
        self.result_es = {'took': 1, 'timed_out': False, '_shards': {'total': 5, 'successful': 5, 'skipped': 0, 'failed': 0}, 'hits': {'total': 1, 'max_score': 10.718289, 'hits': [{'_index': 'monitor', '_type': 'doc', '_id': '2018-06-07_facebook_agency_IES_1145_BR_NULL_installs', '_score': 10.718289, '_source': {'id': '2018-06-07_facebook_agency_IES_1145_BR_NULL_installs', 'value': 95309.0, 'date_time': '2018-06-07', 'ad_platform': 'facebook_agency', 'app_series': 'IES', 'app_id': '1145', 'region': 'BR', 'campaign_tag': 'NULL', 'metric_type': 'installs'}}]}}
        self.get_es = {'_index': 'monitor', '_type': 'doc', '_id': '2018-06-20_facebook_agency_IES_1145_BR_NULL_installs', '_version': 4, 'found': True, '_source': {'value': 7489.0, 'date_time': '2018-06-20', 'ad_platform': 'facebook_agency', 'app_series': 'IES', 'app_id': '1145', 'region': 'BR', 'campaign_tag': 'NULL', 'metric_type': 'installs', 'anomly_score': 4.615879311906664, 'label': 'True'}}
        self.scan_es = [{'_index': 'monitor', '_type': 'doc', '_id': '2018-06-15_facebook_IES_1145_BR_NULL_onlinegid_byappID', '_score': None, '_source': {'id': '2018-06-15_facebook_IES_1145_BR_NULL_onlinegid_byappID', 'value': 1, 'date_time': '2018-06-15', 'ad_platform': 'facebook', 'app_series': 'IES', 'app_id': '1145', 'region': 'BR', 'campaign_tag': 'NULL', 'metric_type': 'onlinegid_byappID', 'anomly_score': 0.0, 'label': 'False'}, 'sort': [10651]}, {'_index': 'monitor', '_type': 'doc', '_id': '2018-06-15_facebook_IES_1145_BR_NULL_impressions_per_campaign', '_score': None, '_source': {'id': '2018-06-15_facebook_IES_1145_BR_NULL_impressions_per_campaign', 'value': 9121559.0, 'date_time': '2018-06-15', 'ad_platform': 'facebook', 'app_series': 'IES', 'app_id': '1145', 'region': 'BR', 'campaign_tag': 'NULL', 'metric_type': 'impressions_per_campaign', 'anomly_score': 3.616943189065545, 'label': 'True'}, 'sort': [14618]}]
        self.dimension_info = {
                              "took": 1,
                              "timed_out": "false",
                              "_shards": {
                                "total": 5,
                                "successful": 5,
                                "skipped": 0,
                                "failed": 0
                              },
                              "hits": {
                                "total": 108547,
                                "max_score": 0,
                                "hits": []
                              },
                              "aggregations": {
                                "date": {
                                  "doc_count": 4152,
                                  "type": {
                                    "doc_count_error_upper_bound": 0,
                                    "sum_other_doc_count": 0,
                                    "buckets": [
                                      {
                                        "key": "IES",
                                        "doc_count": 2558
                                      },
                                      {
                                        "key": "TopBuzz",
                                        "doc_count": 1594
                                      }
                                    ]
                                  }
                                }
                              }
                            }


    @mock.patch('elasticsearch.helpers.scan')
    def test_scanES(self, mock_es):
        """test scanES"""
        mock_es.return_value = [i for i in self.scan_es]
        res1 = scanES(str(self.TO_DATE), self.obj['metric_type'])
        self.assertEqual(2, len(res1))
        mock_es.return_value = [i for i in self.scan_es]
        res3 = scanES(str(self.TO_DATE))
        self.assertEqual(2, len(res3))
        mock_es.return_value = [i for i in []]
        res2 = scanES(str(self.TO_DATE), self.obj['metric_type'])
        self.assertFalse(res2)
        mock_es.return_value = elasticsearch.exceptions.TransportError(404, "wrong", None)
        with self.assertRaises(Exception):
            scanES(str(self.TO_DATE), self.obj)
        print('shortDescription(): ', self.shortDescription())

    @mock.patch('elasticsearch.Elasticsearch.search')
    def test_getraw(self, search):
        """test getraw."""
        # Normal value
        search.return_value = self.result_es
        assump = self.result_es['hits']['hits'][0]['_source']['value']
        res = get_raw(self.TO_DATE.strftime("%Y-%m-%d"), self.obj)
        self.assertEqual(search.call_count, 1)
        self.assertEqual(assump, res)
        # not found any result
        es2 = self.result_es
        es2['hits']['total'] = 0
        search.return_value = es2
        res2 = get_raw(self.TO_DATE.strftime("%Y-%m-%d"), self.obj)
        self.assertEqual(0, res2)
        # Exception:
        search.return_value = TypeError(" cannot search the result")
        with self.assertRaises(Exception):
            get_raw(self.TO_DATE.strftime("%Y-%m-%d"), self.obj)
        print('shortDescription(): ', self.shortDescription())

