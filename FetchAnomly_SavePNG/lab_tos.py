from fetch_true_series import *
from write_tos import *


obj = {
                "ad_platform": "facebook",
                "app_series": "IES",
                "app_id": "1145",
                "region": "IN",
                "campaign_tag": "NULL",
                "metric_type": "installs"
            }
date_series = [i for i in daterange(DATE)]
series = get_series(date_series, obj)
image = bokeh_plot(date_series, series, obj)
# convert png to bytes
from io import BytesIO
imgBuf = BytesIO()
image.save(imgBuf, "PNG")

key = save_to_tos(imgBuf.getvalue(), key=None)
print("key: ", key)
print("success")



# from PIL import Image
# byte_data = imgBuf.getvalue()
# base64_str = base64.b64encode(byte_data)
# img = Image.open(imgBuf)
# print(type(imgBuf.getvalue()))
# img.save("test123455555555.png")
#

