import hashlib

TOS_LF_BUCKET = 'adsources-console'
TOS_LF_ACCESS_KEY = 'B2E3VZUX5WN24LH1M5T0'
TOS_LF_OBJ_PREFIX = 'http://lf1-ttcdn-tos.pstatp.com/obj/adsources-console/'

TOS_CLIENT = None

TOS_API_SERVICE_NAME = "toutiao.tos.tosapi.service.lf"


def _get_tos_client(cluster="default", timeout=30):
    from pytos import tos
    global TOS_CLIENT
    if not TOS_CLIENT:
        TOS_CLIENT = tos.TosClient(bucket=TOS_LF_BUCKET, accessKey=TOS_LF_ACCESS_KEY,
                                   cluster=cluster, service=TOS_API_SERVICE_NAME, timeout=timeout)
    return TOS_CLIENT


def save_to_tos(data, key=None):
    if not key:
        key = hashlib.md5(data).hexdigest()
    client = _get_tos_client()
    client.put_object(key, data)
    return TOS_LF_OBJ_PREFIX + key