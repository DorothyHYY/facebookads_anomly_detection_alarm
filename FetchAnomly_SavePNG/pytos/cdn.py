# -*- coding: utf-8 -*-

import bisect
import hashlib
import struct

ToutiaoCDN = "TT"
XiGuaCDN = "XG"
HuoshanCDN = "HS"
WuKongCDN = "WK"
DouYinCDN = "DY"
FlipgramCDN = "FG"
TuchongCDN = "TC"
NeiHanCDN = "NH"
OtherCDN = "TT"  # use Toutiao

_ALL = (ToutiaoCDN, XiGuaCDN, HuoshanCDN, WuKongCDN, DouYinCDN, FlipgramCDN,
        TuchongCDN, NeiHanCDN)


def get_domains(product, bucket, key):
    """
    使用说明：
    一、product用于确定请求上下文的业务，以便作CDN财务审计，从而选择不同的域名;
    二、get_domains 接口会根据 product / bucket / key 返回一组域名，按顺序用作fallback使用；
    三、访问路径为 http[s]?://{DOMAIN}/(obj|img)/{BUCKET}/{KEY}
    """
    assert product in _ALL
    ret = []
    v = md5uint64(bucket + "/" + key)
    nodes = sf_cdns[product]
    idx = bisect.bisect(nodes, (v, ""))
    for i in range(1000):
        name = nodes[(idx + i) % len(nodes)][1]
        if name in ret:
            continue
        ret.append(name)
        if len(ret) >= 3:
            return ret
    return ret


def get_domains_largefile(product, bucket, key):
    """
    同 get_domains 相关较大的文件专用域名 (平均20M以上)
    """
    assert product in _ALL
    ret = []
    v = md5uint64(bucket + "/" + key)
    nodes = lf_cdns[product]
    idx = bisect.bisect(nodes, (v, ""))
    for i in range(1000):
        name = nodes[(idx + i) % len(nodes)][1]
        if name in ret:
            continue
        ret.append(name)
        if len(ret) >= 3:
            return ret
    return ret


def md5uint64(s):
    if not isinstance(s, bytes):
        s = s.encode("ascii")
    return struct.unpack(">Q", hashlib.md5(s).digest()[:8])[0]


def _parse_cdn(cdns):
    ret = {}
    for n in cdns:
        p, n, w = n
        nodes = ret.get(p) or []
        for i in range(w):
            nodes.append((md5uint64(n + "|" + str(i)), n))
        nodes.sort()
        ret[p] = nodes
    return ret


_sf_cdns = [
    (ToutiaoCDN, "sf1-ttcdn-tos.pstatp.com", 100),
    (ToutiaoCDN, "sf3-ttcdn-tos.pstatp.com", 100),
    (ToutiaoCDN, "sf6-ttcdn-tos.pstatp.com", 100),
    (XiGuaCDN, "sf1-xgcdn-tos.pstatp.com", 100),
    (XiGuaCDN, "sf3-xgcdn-tos.pstatp.com", 100),
    (XiGuaCDN, "sf6-xgcdn-tos.pstatp.com", 100),
    (HuoshanCDN, "sf1-hscdn-tos.pstatp.com", 100),
    (HuoshanCDN, "sf3-hscdn-tos.pstatp.com", 100),
    (HuoshanCDN, "sf6-hscdn-tos.pstatp.com", 100),
    (WuKongCDN, "sf1-wkcdn-tos.pstatp.com", 100),
    (WuKongCDN, "sf3-wkcdn-tos.pstatp.com", 100),
    (WuKongCDN, "sf6-wkcdn-tos.pstatp.com", 100),
    (DouYinCDN, "sf1-dycdn-tos.pstatp.com", 100),
    (DouYinCDN, "sf3-dycdn-tos.pstatp.com", 100),
    (DouYinCDN, "sf6-dycdn-tos.pstatp.com", 100),
    (FlipgramCDN, "sf1-fgcdn-tos.pstatp.com", 100),
    (FlipgramCDN, "sf3-fgcdn-tos.pstatp.com", 100),
    (FlipgramCDN, "sf6-fgcdn-tos.pstatp.com", 100),
    (TuchongCDN, "sf1-tccdn-tos.pstatp.com", 100),
    (TuchongCDN, "sf3-tccdn-tos.pstatp.com", 100),
    (TuchongCDN, "sf6-tccdn-tos.pstatp.com", 100),
    (NeiHanCDN, "sf1-nhcdn-tos.pstatp.com", 100),
    (NeiHanCDN, "sf3-nhcdn-tos.pstatp.com", 100),
    (NeiHanCDN, "sf6-nhcdn-tos.pstatp.com", 100),
]

sf_cdns = _parse_cdn(_sf_cdns)

_lf_cdns = [
    (ToutiaoCDN, "lf1-ttcdn-tos.pstatp.com", 100),
    (ToutiaoCDN, "lf3-ttcdn-tos.pstatp.com", 100),
    (ToutiaoCDN, "lf6-ttcdn-tos.pstatp.com", 100),
    (XiGuaCDN, "lf1-xgcdn-tos.pstatp.com", 100),
    (XiGuaCDN, "lf3-xgcdn-tos.pstatp.com", 100),
    (XiGuaCDN, "lf6-xgcdn-tos.pstatp.com", 100),
    (HuoshanCDN, "lf1-hscdn-tos.pstatp.com", 100),
    (HuoshanCDN, "lf3-hscdn-tos.pstatp.com", 100),
    (HuoshanCDN, "lf6-hscdn-tos.pstatp.com", 100),
    (WuKongCDN, "lf1-wkcdn-tos.pstatp.com", 100),
    (WuKongCDN, "lf3-wkcdn-tos.pstatp.com", 100),
    (WuKongCDN, "lf6-wkcdn-tos.pstatp.com", 100),
    (DouYinCDN, "lf1-dycdn-tos.pstatp.com", 100),
    (DouYinCDN, "lf3-dycdn-tos.pstatp.com", 100),
    (DouYinCDN, "lf6-dycdn-tos.pstatp.com", 100),
    (FlipgramCDN, "lf1-fgcdn-tos.pstatp.com", 100),
    (FlipgramCDN, "lf3-fgcdn-tos.pstatp.com", 100),
    (FlipgramCDN, "lf6-fgcdn-tos.pstatp.com", 100),
    (TuchongCDN, "lf1-tccdn-tos.pstatp.com", 100),
    (TuchongCDN, "lf3-tccdn-tos.pstatp.com", 100),
    (TuchongCDN, "lf6-tccdn-tos.pstatp.com", 100),
    (NeiHanCDN, "lf1-nhcdn-tos.pstatp.com", 100),
    (NeiHanCDN, "lf3-nhcdn-tos.pstatp.com", 100),
    (NeiHanCDN, "lf6-nhcdn-tos.pstatp.com", 100),
]

lf_cdns = _parse_cdn(_lf_cdns)

if __name__ == "__main__":
    for i in range(10):
        print(get_domains("TT", "x", str(i)))
