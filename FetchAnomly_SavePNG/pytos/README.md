# 通用对象存储的Python SDK

### 使用方式

正式环境请使用SCM打包的方式, 打包的部署目录为 `toutiao/lib`。因此实际目录状态为 `toutiao/lib/pytos`

`toutiao/lib` 默认会被加入到PYTHONPATH中

因此使用方式为

```
    from pytos import tos
    ...
```

使用实例见`example`目录
