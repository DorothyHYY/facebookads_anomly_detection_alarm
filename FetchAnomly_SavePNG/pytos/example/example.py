# -*- coding: utf-8 -*-

import sys
import hashlib

sys.path.append("../../")
from pytos import tos

# 申请的Bucket名字
bucket = "tostest"
# 申请的AccessKey
accessKey = "BG8DFYMLM6U44P9KX755"


if __name__ == "__main__":
    obj = "object_name"
    body = b"iamobject"
    # cluster 默认使用 "default"
    client = tos.TosClient(bucket, accessKey, cluster="default", timeout=10)
    client.put_object(obj, body)
    ret = client.head_object(obj)
    print(ret)
    print(client.delete_object(obj))
    print(client.head_object(obj))

    upload_id = client.init_upload(obj)
    client.upload_part(obj, upload_id, "0", b"part0" * (1<<20))
    client.upload_part(obj, upload_id, "1", b"part1" * (1<<20))
    client.upload_part(obj, upload_id, "3", b"part3" * (1<<20))
    client.upload_part(obj, upload_id, "2", b"part2" * (1<<20))
    client.complete_upload(obj, upload_id, ["0", "1", "2", "3"])
    storeMD5 = hashlib.md5(b"part0" * (1<<20) + b"part1" * (1<<20) + b"part2" * (1<<20) + b"part3" * (1<<20)).hexdigest()
    data = client.get_object(obj)
    getMD5 =  hashlib.md5(data).hexdigest()
    print(storeMD5 == getMD5)
