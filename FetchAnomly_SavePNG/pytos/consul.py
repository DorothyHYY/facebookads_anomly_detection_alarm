import random
import json
import sys, os
import logging
from time import time

if sys.version_info[0] == 2:
    from httplib import HTTPConnection
elif sys.version_info[0] == 3:
    from http.client import HTTPConnection

CONSUL_AGENT_HOST = ""
CONSUL_AGENT_HOST = CONSUL_AGENT_HOST or os.environ.get("CONSUL_HTTP_HOST")
CONSUL_AGENT_HOST = CONSUL_AGENT_HOST or os.environ.get("TCE_HOST_IP")
CONSUL_AGENT_HOST = CONSUL_AGENT_HOST or "127.0.0.1"
CONSUL_AGENT_PORT = int(os.environ.get("CONSUL_HTTP_PORT") or 2280)


def consul_translate_(name, timeout):
    conn = HTTPConnection(
        CONSUL_AGENT_HOST, CONSUL_AGENT_PORT, timeout=timeout)
    conn.request("GET", "/v1/lookup/name?name=" + name)
    response = conn.getresponse()
    status = response.status
    data = response.read()
    conn.close()
    if status != 200:
        logging.error("consul: %s %s", status, data.decode("utf8"))
        return []
    return json.loads(data.decode("utf8"))


__cache = dict()


def consul_translate(name, cachetime=3):
    now = time()
    cache = __cache.get(name)
    if cache and now - cache["cachetime"] < cachetime:
        return cache["ret"]
    try:
        ret = consul_translate_(name, timeout=0.1 if cache else 30)
    except:
        if cache:
            return cache["ret"]
        raise
    __cache[name] = dict(cachetime=now, ret=ret)
    return ret


def getone(name, cluster="default", cachetime=3):
    ins = consul_translate(name, cachetime=cachetime)
    addrs = [v for v in ins if v["Tags"].get("cluster") == cluster]
    addr = random.choice(addrs)
    return (addr.get("Host", ""), int(addr.get("Port", 0)))


if __name__ == "__main__":  # example
    CONSUL_AGENT_HOST = "10.6.24.195"
    print(consul_translate("toutiao.tos.imageservice.service.lf"))
    print(consul_getone("toutiao.tos.imageservice.service.lf"))
