# -*- codeing: utf-8 -*-
from __future__ import absolute_import

import random
import sys
import os
import json

from .consul import consul_translate
from .tos import TosException

if sys.version_info[0] == 2:
    PY3 = False
    from httplib import HTTPConnection

elif sys.version_info[0] == 3:
    PY3 = True
    from http.client import HTTPConnection

DEFAULT_TIMEOUT = 10
DEFAULT_CLUSTER = "default"

SERVICE_NAME = "toutiao.imageservice.imageservice"
TOS_ACCESS_HEADER = "x-tos-access"


class TosImageserviceClient(object):
    def __init__(self, bucket, accessKey, **kwargs):
        self.bucket = bucket
        self.access_key = accessKey
        self.timeout = kwargs.get("timeout", DEFAULT_TIMEOUT)
        self.cluster = kwargs.get("cluster", DEFAULT_CLUSTER)

    def _get_addr(self):
        _test_addr = os.environ.get("TEST_IMAGESEVICE_ADDR")
        if _test_addr:
            host_port = _test_addr.split(":")
            return (host_port[0], int(host_port[1]))
        ins = consul_translate(SERVICE_NAME)
        ins = [v for v in ins if v["Tags"].get("cluster") == self.cluster]
        random.shuffle(ins)
        return (ins[0].get("Host", ""), int(ins[0].get("Port", 0)))

    def _get_conn(self):
        addr = self._get_addr()
        return HTTPConnection(addr[0], addr[1], timeout=self.timeout)

    def _req(self, method, url, body=None):
        if body and not isinstance(body, bytes):
            raise TosException(400, "data not bytes", None, None)
        headers = {TOS_ACCESS_HEADER: self.access_key}
        conn = self._get_conn()
        addr = "%s:%d" % (conn.host, conn.port)
        try:
            conn.request(method, url, body, headers)
            response = conn.getresponse()
            status = response.status
            data = response.read()
            conn.close()
        except Exception as e:
            raise TosException(-1, str(e), None, addr)
        if status == 404:
            raise TosException(404, "Not Found", None, addr)
        if status != 200:
            raise TosException(status, data.decode("utf-8"), None, addr)
        headers = response.getheaders()
        if PY3:
            headers = [(k.lower(), v) for k, v in headers]
        return data, dict(headers)

    def getimage(self, key, params):
        uri = "/%s/%s~%s" % (self.bucket, key, params)
        data, headers = self._req("GET", uri)
        return data

    def processimage(self, param, data):
        uri = "/process/%s?bucket=%s" % (param, self.bucket)
        data, headers = self._req("POST", uri, body=data)
        return data

    def imageinfo(self, key):
        uri = "/%s/%s~%s" % (self.bucket, key, "info")
        data, headers = self._req("GET", uri)
        return json.loads(data)


TEST_BUCKET = "tostest"
TEST_AK = "BG8DFYMLM6U44P9KX755"
testclient = TosImageserviceClient(
    TEST_BUCKET, TEST_AK, cluster="default", timeout=2)
