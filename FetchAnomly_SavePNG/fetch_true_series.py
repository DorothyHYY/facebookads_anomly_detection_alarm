import datetime
from elasticsearch import Elasticsearch
from concurrent.futures import ThreadPoolExecutor
import elasticsearch
from elasticsearch import helpers
from copy import copy
import os
import yaml
from overwrite_bokeh_funcs import convert_png
from write_tos import *



########################################################################################################################
###################################################### UserCostomize ###################################################
########################################################################################################################
DATE = datetime.date(2018,6,23)
# Setting configuration
config = os.path.join(os.path.dirname(__file__), "config.yaml")
with open(config, "r") as f:
    settings = yaml.load(f)
ES_IP = settings['elasticsearch']['ip']
ES_PORT = settings['elasticsearch']['port']
# instantiate es client, connects to localhost:9200 by default
es = Elasticsearch(ES_IP, port=ES_PORT)
# Config series length for alarms:
DATE_LENGTH = settings['date_length']
########################################################################################################################
############################################# interaction Elasticsearch ################################################
########################################################################################################################
# Fetch all true label data in the specific date
def scanES(date, metric_type=None):
    """
    :param date:
    :return: obj contains ad_platform, app_series, app_id, region, campaign_tag, metric_type
    """
    try:
        if metric_type != None:
            body = {
                "query": {
                    "bool": {
                        "must": [
                            {
                                "match": {
                                    "metric_type": metric_type
                                }
                            },
                            {
                                "match": {
                                    "label": "True"
                                }
                            },
                            {
                                "match": {
                                    "date_time": date
                                }
                            }
                        ]
                    }
                }
            }
        else:
            body = {
                  "query":{
                    "bool":{
                      "must":[
                      {
                          "match": {
                              "label": "True"
                          }
                      },
                      {
                        "match": {
                          "date_time": date
                        }
                      }
                      ]
                    }
                  }
                }
        res = helpers.scan(
            es,
            index="monitor",
            doc_type="doc",
            query=body
        )
        exist = list(res)
        obj_list = []
        for type in exist:
            obj = {
                "ad_platform": type["_source"]["ad_platform"],
                "app_series": type["_source"]["app_series"],
                "app_id": type["_source"]["app_id"],
                "region": type["_source"]["region"],
                "campaign_tag": type["_source"]["campaign_tag"],
                "metric_type": type["_source"]["metric_type"]
            }
            obj_list.append(copy(obj))
        if len(exist) != 0 and len(obj_list) != 0:
            return obj_list
        else:
            return False
    except (elasticsearch.exceptions.TransportError, Exception) as err:
        raise Exception(err)

# fetch a series data for each true label
# Get one:
def get_raw(DATE,obj):
    try:
        result = es.search(
            index='monitor',
            body={
                  "query":{
                    "bool":{
                      "must":[{
                        "match": {
                          "ad_platform": obj["ad_platform"]
                        }
                      },
                      {
                        "match": {
                          "app_id": obj["app_id"]
                        }
                      },{
                        "match": {
                          "campaign_tag": obj["campaign_tag"]
                        }
                      },{
                        "match": {
                          "region": obj["region"]
                        }
                      },{
                        "match": {
                          "metric_type": obj["metric_type"]
                        }
                      },
                      {
                        "match": {
                          "date_time": DATE
                        }
                      }
                      ]
                    }
                  }
                }
        )
        print(result)
        if result['hits']['total'] != 0:
            return result['hits']['hits'][0]['_source']['value']
        else:
            # TODO: if a metric type in some period has value while some period has no value, it shoule be an abnomal, we give the value to 0.
            return 0
        # return result
    except Exception as err:
        print("err")
        raise Exception(err)

# Get series
# Use ThreadPoolExecutor to deal with concurrency, speed up the process:
def get_series(date_range, obj):
    work_num = 14
    with ThreadPoolExecutor(max_workers=work_num) as exector:
        future_list = []
        for date in date_range:
            future = exector.submit(get_raw, date, obj)
            future_list.append(future)
        result = []
        # yield Future
        for future in future_list:
            # fetch result
            res = future.result()
            result.append(res)
    return result


# Given length and TO_DATE, generate a daterange:
def daterange(TO_DATE):
    FROM_DATE = TO_DATE - datetime.timedelta(DATE_LENGTH - 1)
    for n in range(DATE_LENGTH):
        yield str(FROM_DATE + datetime.timedelta(n))

########################################################################################################################
################################################ Bokeh save PNG ########################################################
########################################################################################################################
import pandas as pd
from bokeh.plotting import figure, ColumnDataSource
from bokeh.models.formatters import NumeralTickFormatter


def bokeh_plot(date_list, series, obj):
    annotation = str(obj["ad_platform"]) + "_" + str(obj["app_series"]) + "_" + str(obj["app_id"]) + "_" + str(obj["region"]) + "_" + str(obj["campaign_tag"]) + "_" + str(obj["metric_type"])
    TOOLS = "hover,crosshair,pan,wheel_zoom,zoom_in,zoom_out,box_zoom,undo,redo,reset,tap,save,box_select,poly_select,lasso_select,"
    p = figure(tools=TOOLS, plot_width=1000, plot_height=500, x_axis_type="datetime")
    for time, value, types, colors in zip([pd.to_datetime(date_list), pd.to_datetime([date_list[-1]])], [series, [series[-1]]],[annotation + "-normal", annotation + "-abnormal"],["#3288bd", "#d53e4f"]):
        data_dict = {"Time": time,
                     "Value": value,
                     "Type": types
                     }

        data = pd.DataFrame(data_dict, columns=['Time', 'Value', 'Type'])
        source = ColumnDataSource(data)
        # Draw scatter plot.
        if types == annotation + "-normal":
            if len(time) != len(value):
                raise Exception("length of time and value are not equal")
            p.circle(x='Time', y='Value', source=source, color=colors, size=20, fill_alpha=0.8, line_color=None,
                     legend='Type')
            p.line(x='Time', y='Value', source=source, color=colors, line_alpha=0.8, line_width=0.5, legend='Type')
        else:
            p.circle(x='Time', y='Value', source=source, color=colors, size=20, fill_alpha=1.5, line_color=None,
                     legend='Type')
    p.legend.location = "top_left"
    p.xaxis.axis_label = 'Time'
    p.yaxis.axis_label = 'Value'
    p.yaxis.formatter = NumeralTickFormatter(format='0,0.00')
    image = convert_png(p)
    return image

def write_to_tos(DATE):
    try:
        all_true_obj = scanES(str(DATE), "installs")
        date_series = [i for i in daterange(DATE)]
        # Store image url
        key_list = []
        # Use to store dimensions' info
        obj_list = []
        for obj in all_true_obj:
            series = get_series(date_series, obj)
            image = bokeh_plot(date_series, series, obj)
            # Convert png to bytes
            from io import BytesIO
            imgBuf = BytesIO()
            image.save(imgBuf, "PNG")
            key = save_to_tos(imgBuf.getvalue(), key=None)
            print("key: ", key)
            key_list.append(key)
            obj_list.append(obj)
            line = "key," + str(key)
            template = "data/key_" + str(DATE) + ".csv"
            with open(template, "a") as file:
                file.write(line)
                file.write('\n')
        return key_list, obj_list
    except Exception as err:
        raise err

########################################################################################################################
###################################################### MainFunc ########################################################
########################################################################################################################
# keylist = write_to_tos(datetime.date(2018,6,23))
# print(keylist)
