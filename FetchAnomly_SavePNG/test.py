import json
import requests
import datetime
REQUESTURL = "https://oapi.zjurl.cn/open-apis/api/v2/message"
BOT_TOKEN = "b-e68b64bc-ac90-4006-bdba-3d314cd00436"
today = datetime.datetime.today().strftime("%Y-%m-%d")
title = today + "请点击消息查看facebook ads 异常警报"
HEADERS = {
    'Content-Type': 'application/json;charset=UTF-8'
}


def alarm_image_text_lark(key_list, obj_list):
    try:
        for image_url,obj in zip(key_list, obj_list):
            data = {
                    "token": BOT_TOKEN,
                    #"email": "huangyingying.doro@bytedance.com",
                    "msg_type": "post",
                    "chat_id":"6574965963093967118",
                    "content": {
                        "text": "<p>异常情况报警</p><p>platform: "+str(obj["ad_platform"])+" series: "+str(obj["app_series"])+" id: "+str(obj["app_id"])+" campaign_tag: "+str(obj["campaign_tag"])+" region: "+str(obj["region"])+" metric_type: "+str(obj["metric_type"])+"</p><p>image_irl: "+str(image_url)+"</p><figure><img src=\""+str(image_url)+"\" origin-width=\"1280\" origin-height=\"640\"/></figure>",
                        "title": title
                        }
                    }
            data = json.dumps(data)
            print(data)
            response = requests.post(REQUESTURL, headers=HEADERS, data=data)
            print(response.json())
        return True
    except Exception as err:
        raise Exception(err)



###################################################### TestFunc ########################################################
########################################################################################################################
keylist = ["http://lf1-ttcdn-tos.pstatp.com/obj/adsources-console/9ba7a8dc7b1ec09a36b23510b6161c51"]
obj = {
    "ad_platform": "facebook",
    "app_series": "IES",
    "app_id": "1145",
    "campaign_tag": "NULL",
    "region": "US",
    "metric_type": "installs"
}
objlist = [obj]
response = alarm_image_text_lark(keylist, objlist)
print(response)