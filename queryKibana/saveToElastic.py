from datetime import datetime
from elasticsearch import Elasticsearch, helpers

es = Elasticsearch()

doc = {
    'value': 12345,
    'date_time': '2018-06-07',
    'ad_platform': 'facebook_agency',
    'app_series': 'IES',
    'app_id': '123',
    'region': 'CN',
    'campaign_tag': 'NULL',
    'metric_type': 'installs'
}

# instantiate es client
es = Elasticsearch(['10.11.40.134', '10.11.40.135', '10.11.40.194', '10.11.40.195'], port=9200)


def indexES(doc,id):
    try:
        es.index(index="monitor", doc_type='doc', id=id, body=doc)
        return True
    except Exception as err:
        print(err)
        return False

def queryES():
    try:
        #es.indices.refresh(index="monitor")
        res = es.search(index="monitor", body={"query": {"match_all": {}}})
        print(res)
        return res
    except Exception as err:
        print(err)
        return False

def encode_monitor(doc, obj_dic):
    doc_template = ["date_time", "ad_platform", "app_series", "app_id", "region", "campaign_tag"]
    for item in doc_template:
        if item not in doc:
            raise Exception("Requirements are not satisfied, ", item, "is lost.")
    # obj_dic = ["installs", "clicks", "impressions", "cost", "CTR", "CVR", "ResultRate", "CPM", "CPA", "impressions_per_campaign", "impressions_per_account", "onlinegid_byappID", "impressions_byappid", "accounts_byappid", "installperaccount_byappid", "installspercampaign_byadplatform", "installsperacc_weeklychange", "resulterate_bycamtag"]
    for keywords, value in obj_dic.items():
        if value != None:
            idx = doc['date_time'] + "_" + doc['ad_platform'] + "_" + doc['app_series'] + "_" + doc['app_id'] + "_" + doc['region'] + "_" + doc['campaign_tag'] + "_" + keywords
            es_fields_keys = ('value', 'date_time', 'ad_platform', 'app_series', 'app_id', 'region', 'campaign_tag', 'metric_type')
            es_fields_vals = (value, doc['date_time'], doc['ad_platform'], doc['app_series'], doc['app_id'], doc['region'], doc['campaign_tag'], keywords)

            # Return a dict holding values from each doc:
            es_monitor_d = dict(zip(es_fields_keys, es_fields_vals))
            # Return the row on each iteration
            yield idx, es_monitor_d
        else:
            yield None, None



def es_add_bulk(doc, obj_dict, thread_count, queue_size):
    # This is for a generator.
    if doc == {} or obj_dict == {}:
        return False
    action = ({
            "_index": "monitor",
            "_type" : "doc",
            "_id"   : idx,
            "_source": es_monitor_d,
         } for idx, es_monitor_d in encode_monitor(doc, obj_dict) if idx != None)
    try:
        cur_num = 0
        fault_tolerance = 20
        fault = 0
        obj_list = list(obj_dict.values())
        for ok, result in helpers.parallel_bulk(es, action, thread_count=thread_count, queue_size=queue_size):
            # If fails, retry if within fault_tolerance
            while not result and fault < fault_tolerance:
                fault += 1
                print("fail", obj_list[cur_num])
                action = ({
                    "_index": "monitor",
                    "_type": "doc",
                    "_id": idx,
                    "doc": es_monitor_d,
                } for idx, es_monitor_d in encode_monitor(doc, [obj_list[cur_num]]))
                r = [res for ok, res in
                     helpers.parallel_bulk(es, action, thread_count=thread_count, queue_size=queue_size,
                                           request_timeout=30)]
                result = r[0]
            if fault >= fault_tolerance and not result:
                return False
            cur_num += 1
        return True
    except (helpers.ElasticsearchException, Exception) as err:
        print(err)
        return False


