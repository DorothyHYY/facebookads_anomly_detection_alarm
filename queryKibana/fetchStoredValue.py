from elasticsearch import Elasticsearch
DEBUG = False
DEBUG_PRINT = False
DATE = "2018-06-07"


# instantiate es client, connects to localhost:9200 by default
es = Elasticsearch(['10.11.40.134', '10.11.40.135', '10.11.40.194', '10.11.40.195'], port=9200)


##################################### Fetch dimensions' content #######################################################

def fetchData(var, DATE):
    result = es.search(
        index='automatic_ads_report_fb_' + DATE,
        body={
          "size": 0,
          "aggs" : {
            "res" : {
                "terms" : { "field" : var,  "size" : 500 }
            }
          }
        }
    )
    res = get_list(result)
    return res

def get_list(result):
    if DEBUG:
        print(result)
        print(type(result))
    res = []
    for item in result['aggregations']['res']['buckets']:
        if DEBUG:
            print("item")
        res.append(item["key"])
    return res

##################################### Calling example #######################################################
def debug_print():
    PLATFORM = fetchData('ad_platform',DATE)
    APPID = fetchData('app_id',DATE)
    APPSERIES = fetchData('app_series',DATE)
    CAMPAIGNTAG = fetchData('campaign_tag',DATE)
    REGIONS = fetchData('region',DATE)
    print("=" * 80)
    print("Platform = ", PLATFORM)
    print("=" * 80)
    print("app id = ", APPID)
    print(len(APPID))
    print("=" * 80)
    print("app series = ", APPSERIES)
    print("=" * 80)
    print("campaign tag = ", CAMPAIGNTAG)
    print("=" * 80)
    print("region = ", REGIONS)
    print(len(REGIONS))


if DEBUG_PRINT == True:
    debug_print()


##################################### Fetch raw data by query elasticsearch #######################################################

def get_raw(DATE, obj):
    result = es.search(
        index='automatic_ads_report_fb_' + DATE,
        body={
            "size": 0,
              "query": {
                "bool": {
                  "must":[
                    {
                      "match": {
                        "app_id": obj["id"]
                      }
                    },{
                      "match": {"ad_platform": obj["platform"]}
                    },
                    {
                      "match": {"campaign_tag": obj["campaign_tag"]}
                    },
                  {
                      "match": {"region": obj["region"]}
                  }
                  ],
                  "filter": {
                    "query_string": {
                        "query": obj["series"]
                    }
                  }
                }
              },
            "aggs": {
                "result": {
                    "terms": {"script": "'trick'"},
                    "aggs": {
                        "sum_clicks": {
                            "sum": {
                                "field": "clicks"
                            }
                        },
                        "sum_impressions": {
                            "sum": {
                                "field": "impressions"
                            }
                        },
                        "sum_installs": {
                            "sum": {
                                "field": "installs"
                            }
                        },
                        "sum_cost": {
                            "sum": {
                                "field": "cost"
                            }
                        },
                        "camgid_num": {
                            "cardinality": {
                                "field": "campaign_gid"
                            }
                        },
                        "adaccountid_num": {
                            "cardinality": {
                                "field": "ad_account_id"
                            }
                        },
                        "CTR": {
                            "bucket_script": {
                                "buckets_path": {
                                    "var1": "sum_clicks",
                                    "var2": "sum_impressions"
                                },
                                "script": "params.var1 / params.var2"
                            }
                        },
                        "CVR":{
                            "bucket_script": {
                                "buckets_path": {
                                    "var1": "sum_installs",
                                    "var2": "sum_clicks"
                                },
                                "script": "params.var1 / params.var2"
                            }
                        },
                        "ResultRate": {
                            "bucket_script": {
                                "buckets_path": {
                                    "var1": "sum_installs",
                                    "var2": "sum_impressions"
                                },
                                "script": "params.var1 / params.var2"
                            }
                        },
                        "CPM": {
                            "bucket_script": {
                                "buckets_path": {
                                    "var1": "sum_cost",
                                    "var2": "sum_impressions"
                                },
                                "script": "params.var1 * 1000 / params.var2"
                            }
                        },
                        "CPA": {
                            "bucket_script": {
                                "buckets_path": {
                                    "var1": "sum_cost",
                                    "var2": "sum_installs"
                                },
                                "script": "params.var1 / params.var2"
                            }
                        },
                        "ImprePerCam": {
                            "bucket_script": {
                                "buckets_path": {
                                    "var1": "sum_impressions",
                                    "var2": "camgid_num"
                                },
                                "script": "params.var1 / params.var2"
                            }
                        },
                        "ImprePerAcc": {
                            "bucket_script": {
                                "buckets_path": {
                                    "var1": "sum_impressions",
                                    "var2": "adaccountid_num"
                                },
                                "script": "params.var1 / params.var2 / 1000"
                            }
                        }
                    }
                }
            }
        }
    )
    return result

def get_raw_byAppID(DATE, obj):
    result = es.search(
        index='automatic_ads_report_fb_' + DATE,
        body={
            "size": 0,
              "query": {
                "bool": {
                  "must":[
                    {
                      "match": {
                        "app_id": obj["id"]
                      }
                    },{
                      "match": {"ad_platform": obj["platform"]}
                    },
                    {
                      "match": {"campaign_tag": obj["campaign_tag"]}
                    },
                  {
                      "match": {"region": obj["region"]}
                  }
                  ],
                  "filter": {
                    "query_string": {
                        "query": obj["series"]
                    }
                  }
                }
              },
            "aggs": {
                "byAppID": {
                    "terms": {"field": "app_id"},
                    "aggs": {
                        "sum_impressions": {
                            "sum": {
                                "field": "impressions"
                            }
                        },
                        "sum_installs": {
                            "sum": {
                                "field": "installs"
                            }
                        },
                        "onlinegid_byAppID": {
                            "cardinality": {
                                "field": "campaign_gid"
                            }
                        },
                        "accounts_appID": {
                            "cardinality": {
                                "field": "ad_account_id"
                            }
                        },
                        "installsPerAcc_byAppID": {
                            "bucket_script": {
                                "buckets_path": {
                                    "var1": "sum_installs",
                                    "var2": "accounts_appID"
                                },
                                "script": "params.var1 / params.var2"
                            }
                        }
                    }
                }
            }
            }
    )
    return result


def get_raw_byPlat(DATE, obj):
    result = es.search(
        index='automatic_ads_report_fb_' + DATE,
        body={
            "size": 0,
              "query": {
                "bool": {
                  "must":[
                    {
                      "match": {
                        "app_id": obj["id"]
                      }
                    },{
                      "match": {"ad_platform": obj["platform"]}
                    },
                    {
                      "match": {"campaign_tag": obj["campaign_tag"]}
                    },
                  {
                      "match": {"region": obj["region"]}
                  }
                  ],
                  "filter": {
                    "query_string": {
                        "query": obj["series"]
                    }
                  }
                }
              },
            "aggs": {
                "byPlat": {
                    "terms": {"field": "ad_platform"},
                    "aggs": {
                        "sum_installs": {
                            "sum": {
                                "field": "installs"
                            }
                        },
                        "campaign_byPlat": {
                            "cardinality": {
                                "field": "campaign_id"
                            }
                        },
                        "accounts_byPlat": {
                            "cardinality": {
                                "field": "ad_account_id"
                            }
                        },
                        "installsPerCam_byPlat": {
                            "bucket_script": {
                                "buckets_path": {
                                    "var1": "sum_installs",
                                    "var2": "campaign_byPlat"
                                },
                                "script": "params.var1 / params.var2"
                            }
                        },
                        "installsPerAcc_weeklychange": {
                            "bucket_script": {
                                "buckets_path": {
                                    "var1": "sum_installs",
                                    "var2": "accounts_byPlat"
                                },
                                "script": "params.var1 / params.var2"
                            }
                        }
                    }
                }
            }
        }
    )
    return result


def get_raw_byCamTag(DATE, obj):
    result = es.search(
        index='automatic_ads_report_fb_' + DATE,
        body={
            "size": 0,
              "query": {
                "bool": {
                  "must":[
                    {
                      "match": {
                        "app_id": obj["id"]
                      }
                    },{
                      "match": {"ad_platform": obj["platform"]}
                    },
                    {
                      "match": {"campaign_tag": obj["campaign_tag"]}
                    },
                  {
                      "match": {"region": obj["region"]}
                  }
                  ],
                  "filter": {
                    "query_string": {
                        "query": obj["series"]
                    }
                  }
                }
              },
            "aggs": {
                "byCamTag": {
                    "terms": {"field": "campaign_tag"},
                    "aggs": {
                        "sum_installs": {
                            "sum": {
                                "field": "installs"
                            }
                        },
                        "sum_impressions": {
                            "sum": {
                                "field": "impressions"
                            }
                        },
                        "RR_byCamTag": {
                            "bucket_script": {
                                "buckets_path": {
                                    "var1": "sum_installs",
                                    "var2": "sum_impressions"
                                },
                                "script": "params.var1 / params.var2"
                            }
                        }
                    }
                }
            }
        }
    )
    return result

##################################### Processing result #######################################################

def fetch_result(re):
    hits = re['hits']['total']
    if hits == 0:
        return False, 0,0,0,0,0,0,0,0,0,0,0
    else:
        result = re['aggregations']['result']
        installs = result['buckets'][0]['sum_installs']['value']
        clicks = result['buckets'][0]['sum_clicks']['value']
        impressions = result['buckets'][0]['sum_impressions']['value']
        cost = result['buckets'][0]['sum_cost']['value']
        CTR = result['buckets'][0]['CTR']['value']
        CVR = result['buckets'][0]['CVR']['value']
        RR = result['buckets'][0]['ResultRate']['value']
        CPM = result['buckets'][0]['CPM']['value']
        CPA = result['buckets'][0]['CPA']['value']
        ImprePerCam = result['buckets'][0]['ImprePerCam']['value']
        ImprePerAcc = result['buckets'][0]['ImprePerAcc']['value']
        return True, installs, clicks, impressions, cost, CTR, CVR, RR, CPM, CPA, ImprePerCam, ImprePerAcc


def fetch_result_GroupByID(re):
    hits = re['hits']['total']
    if hits == 0:
        return False, None,None,None,None
    else:
        result = re['aggregations']['byAppID']
        onlinegid_byAppID = result['buckets'][0]['onlinegid_byAppID']['value']
        total_impressions = result['buckets'][0]['sum_impressions']['value']
        accounts_appID = result['buckets'][0]['accounts_appID']['value']
        installsPerAcc_byAppID = result['buckets'][0]['installsPerAcc_byAppID']['value']

        return True, onlinegid_byAppID, total_impressions, accounts_appID, installsPerAcc_byAppID

def fetch_result_GroupByPlat(re):
    hits = re['hits']['total']
    if hits == 0:
        return False, None,None
    else:
        result = re['aggregations']['byPlat']
        installsPerCam_byPlat = result['buckets'][0]['installsPerCam_byPlat']['value']
        installsPerAcc_weeklychange = result['buckets'][0]['installsPerAcc_weeklychange']['value']
        return True, installsPerCam_byPlat,installsPerAcc_weeklychange


def fetch_result_GroupByCamTag(re):
    hits = re['hits']['total']
    if hits == 0:
        return False, None
    else:
        result = re['aggregations']['byCamTag']
        RR_byCamTag = result['buckets'][0]['RR_byCamTag']['value']
        return True, RR_byCamTag



##################################### Packaging query and processing procedures #######################################################

def index(DATE, obj):
    result = get_raw(DATE, obj)
    if DEBUG:
        print(result)
    return fetch_result(result)


def indexGroupByAppid(DATE, obj):
    result = get_raw_byAppID(DATE, obj)
    if DEBUG:
        print(result)
    return fetch_result_GroupByID(result)

def indexGroupByPlat(DATE, obj):
    result = get_raw_byPlat(DATE, obj)
    if DEBUG:
        print(result)
    return fetch_result_GroupByPlat(result)

def indexGroupByCamTag(DATE, obj):
    result = get_raw_byCamTag(DATE, obj)
    if DEBUG:
        print(result)
    return fetch_result_GroupByCamTag(result)

