import unittest
import os
from InteractKibana.queryKibana.saveToElastic import *
from InteractKibana.queryKibana.fetchStoredValue import *
from unittest import mock
import numpy as np
import datetime
import elasticsearch




class MyTestCase(unittest.TestCase):
    def setUp(self):
        self.doc1 = {
            "date_time": "2018-06-12",
            "ad_platform": "facebook",
            "app_series": "IES",
            "app_id": "1180",
            "region": "US",
            "campaign_tag": "NULL"
        }
        self.obj_dict = {
            "installs": 10000,
            "ctr": 8.2,
            "CVR": 0.08,
            "ResultRate": 1.2
        }
        self.obj_none_dict = {
            "installs": None
        }
        self.score_list = [
            {
                "date_time": "2018-06-12",
                "ad_platform": "facebook",
                "app_series": "IES",
                "app_id": "1180",
                "region": "US",
                "campaign_tag": "NULL",
                "metric_type": "install",
                "anomly_score": 0.08,
                "label": "True"
            }]

    def test_encode_monitor(self):
        """encode_monitor with legal input"""
        idx_list = []
        doc_lst = []
        for idx, obj in encode_monitor(self.doc1, self.obj_dict):
            keywords = []
            value = []
            for key, val in obj.items():
                keywords.append(key)
                value.append(val)
            self.assertEqual(len(keywords), 8)
            id = self.doc1["date_time"] + "_" + self.doc1["ad_platform"] + "_" + self.doc1["app_series"] + "_" + self.doc1[
                "app_id"] + "_" + self.doc1["region"] + "_" + self.doc1["campaign_tag"] + "_" + obj["metric_type"]
            self.assertEqual(idx, id)
            if obj["metric_type"] == "install":
                self.assertEqual(obj["value"], self.obj_dict["install"])
            idx_list.append(idx)
            doc_lst.append(doc_lst)
        self.assertEqual(len(idx_list), len(list(self.obj_dict)))
        for idx, obj in encode_monitor(self.doc1, self.obj_none_dict):
            self.assertEqual(None, idx)
            self.assertEqual(None, obj)
        exception_doc = {
            "ad_platform": "facebook",
            "app_series": "IES",
            "app_id": "1180",
            "region": "US",
            "campaign_tag": "NULL"
        }
        with self.assertRaises(Exception):
           list(encode_monitor(exception_doc, self.obj_dict))
        print('shortDescription(): ', self.shortDescription())

    @mock.patch('elasticsearch.helpers.parallel_bulk')
    def test_es_add_bulk(self, mock_bulk):
        """test es_add_bulk with legal input"""
        mock_bulk.return_value=[[True, True] for _ in range(len(self.obj_dict))]
        res = es_add_bulk(self.doc1, self.obj_dict, 4, 4)
        self.assertEqual(mock_bulk.call_count, 1)
        self.assertTrue(res)
        print('shortDescription(): ', self.shortDescription())

    @mock.patch('elasticsearch.helpers.parallel_bulk')
    def test_es_update_bulk_fail(self, mock_bulk):
        """test es_add_bulk, expect es fails first then success"""
        bulk_result =  [[True, True]] * 2
        bulk_result[0] = [True, False]
        mock_bulk.return_value = (i for i in bulk_result)
        res = es_add_bulk(self.doc1, self.obj_dict, 4, 4)
        self.assertTrue(res)
        print('shortDescription(): ', self.shortDescription())

    @mock.patch('elasticsearch.helpers.parallel_bulk')
    def test_es_add_bulk_alwaysfail(self, mock_bulk):
        """test es_add_bulk with, expect to be failed"""
        mock_bulk.return_value = [[True, False] for _ in range(len(self.obj_dict))]
        res = es_add_bulk(self.doc1, self.obj_dict, 4, 4)
        self.assertEqual(mock_bulk.call_count, 21)
        self.assertFalse(res)
        print('shortDescription(): ', self.shortDescription())

    @mock.patch('elasticsearch.helpers.parallel_bulk')
    def test_illegal_es_add_bulk_empty(self, mock_bulk):
        """test es_add_bulk with empty input ; with none list-type input ; with thread count as 0 """
        res = es_add_bulk(self.doc1,{}, 1, 1)
        mock_bulk.assert_not_called()
        self.assertFalse(res)
        res_false1 = es_add_bulk({}, self.obj_dict, 1, 1)
        self.assertFalse(res_false1)
        res_false2 = es_add_bulk(self.doc1, {}, 0, 1)
        self.assertFalse(res_false2)
        print('shortDescription(): ', self.shortDescription())

    @mock.patch('elasticsearch.helpers.parallel_bulk')
    def test_exception_es_add_bulk_exception(self, mock_bulk):
        """test es_add_bulk with exception """
        mock_bulk.side_effect = elasticsearch.exceptions.TransportError(404, "wrong", None)
        res =es_add_bulk(self.doc1, self.obj_dict, 1, 1)
        self.assertFalse(res)
        print('shortDescription(): ', self.shortDescription())

    @mock.patch('elasticsearch.Elasticsearch.index')
    def test_indexES(self, mock_index):
        """test indexES """
        mock_index.return_value = True
        res = indexES(self.doc1, '1')
        self.assertTrue(res)
        mock_index.side_effect = Exception
        res1 = indexES(self.doc1, '1')
        self.assertFalse(res1)
        print('shortDescription(): ', self.shortDescription())

    @mock.patch('elasticsearch.Elasticsearch.search')
    def test_queryES(self, mock_search):
        """test indexES """
        mock_search.return_value = True
        queryES()
        mock_search.assert_called_once()
        mock_search.side_effect = Exception
        res2 = queryES()
        self.assertFalse(res2)
        print('shortDescription(): ', self.shortDescription())


class MyTest_fetch_stored_value(unittest.TestCase):
    def setUp(self):
        self.doc1 = {
            "date_time": "2018-06-12",
            "ad_platform": "facebook",
            "app_series": "IES",
            "app_id": "1180",
            "region": "US",
            "campaign_tag": "NULL"
        }
        self.obj_dict = {
            "installs": 10000,
            "ctr": 8.2,
            "CVR": 0.08,
            "ResultRate": 1.2
        }
        self.obj_none_dict = {
            "installs": None
        }
        self.score_list = [
            {
                "date_time": "2018-06-12",
                "ad_platform": "facebook",
                "app_series": "IES",
                "app_id": "1180",
                "region": "US",
                "campaign_tag": "NULL",
                "metric_type": "install",
                "anomly_score": 0.08,
                "label": "True"
            }]

