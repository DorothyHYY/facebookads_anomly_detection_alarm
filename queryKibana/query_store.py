# from requestKibana import *
import os
import sys

from fetchStoredValue import *
from saveToElastic import *
import time
import datetime
import json
DEBUG = True
DEBUG_TOFILE = True

##################################### CONSTANT SETTINGS ############################################
THREAD_COUNT = 6
QUEUE_SIZE = 2
# DATE = '2018-06-06'
##############################################################################################################################################################
############################# Config_index and indexToElastic are functions that index to elasticsearch one by one ###########################################
################################# These two has been abandoned since we use bulk to speed up our process instead #############################################
##############################################################################################################################################################
# def config_index(doc, value, type, err_count):
#     doc['value'] = value
#     doc['metric_type'] = type
#     doc['id'] = doc['date_time'] + "_" + doc['ad_platform'] + "_" + doc['app_series'] + "_" + doc['app_id'] + "_" + doc['region'] + "_" + doc['campaign_tag'] + "_" + doc['metric_type']
#     id = doc['id']
#     # Index to Elasticsearch:
#     r = indexES(doc,id)
#     fault_tolerance = 20
#     fault = 0
#     while not r and fault < fault_tolerance:
#         err_count += 1
#         fault += 1
#         r = indexES(doc,id)
#     return err_count
#
#
# def indexToElastic(obj, doc):
#     err_count = 0
#     if DEBUG:
#         index_counting = {
#             "install_count": 0,
#             "click_count": 0,
#             "impression_count": 0,
#             "cost_count": 0,
#             "ctr_count": 0,
#             "cvr_count": 0,
#             "rr_count": 0,
#             "cpm_count": 0,
#             "cpa_count": 0,
#             "ipc_count": 0,
#             "ipa_count": 0,
#             "ogid_count": 0,
#             "imaid_count": 0,
#             "accaid_count": 0,
#             "inaid_count": 0,
#             "inpla_count": 0,
#             "inacc_count": 0,
#             "rrcam_count": 0,
#             "err_count": err_count
#         }
#     # Index installs to Elasticsearch:
#     if obj["installs"]:
#         err_count = config_index(doc, obj["installs"], 'installs', err_count)
#         if DEBUG:
#             index_counting["install_count"] += 1
#     # Index clicks to Elasticsearch:
#     if obj["clicks"]:
#         err_count = config_index(doc, obj["clicks"], 'clicks', err_count)
#         if DEBUG:
#             index_counting["click_count"] += 1
#     # Index impressions to Elasticsearch:
#     if obj["impressions"]:
#         err_count = config_index(doc, obj["impressions"], 'impressions', err_count)
#         if DEBUG:
#             index_counting["impression_count"] += 1
#     # Index cost to Elasticsearch:
#     if obj["cost"]:
#         err_count = config_index(doc, obj["cost"], 'cost', err_count)
#         if DEBUG:
#             index_counting["cost_count"] += 1
#     # Index CTR to Elasticsearch:
#     if obj["CTR"]:
#         err_count = config_index(doc, obj["CTR"], 'CTR', err_count)
#         if DEBUG:
#             index_counting["ctr_count"] += 1
#     # Index CVR to Elasticsearch:
#     if obj["CVR"]:
#         err_count = config_index(doc, obj["CVR"], 'CVR', err_count)
#         if DEBUG:
#             index_counting["cvr_count"] += 1
#     # Index RR to Elasticsearch:
#     if obj["RR"]:
#         err_count = config_index(doc, obj["RR"], 'ResultRate', err_count)
#         if DEBUG:
#             index_counting["rr_count"] += 1
#     # Index CPM to Elasticsearch:
#     if obj["CPM"]:
#         err_count = config_index(doc, obj["CPM"], 'CPM', err_count)
#         if DEBUG:
#             index_counting["cpm_count"] += 1
#     # Index CPA to Elasticsearch:
#     if obj["CPA"]:
#         err_count = config_index(doc, obj["CPA"], 'CPA', err_count)
#         if DEBUG:
#             index_counting["cpa_count"] += 1
#     # Index imprePerCam to Elasticsearch:
#     if obj["imprePerCam"]:
#         err_count = config_index(doc, obj["imprePerCam"], 'impressions_per_campaign', err_count)
#         if DEBUG:
#             index_counting["ipc_count"] += 1
#     # Index imprePerAcc to Elasticsearch:
#     if obj["imprePerAcc"]:
#         err_count = config_index(doc, obj["imprePerAcc"], 'impressions_per_account', err_count)
#         if DEBUG:
#             index_counting["ipa_count"] += 1
#     # Index onlineGid to Elasticsearch:
#     if obj["onlinegid"]:
#         err_count = config_index(doc, obj["onlinegid"], 'onlinegid_byappID', err_count)
#         if DEBUG:
#             index_counting["ogid_count"] += 1
#     # Index impressions_byappid to Elasticsearch:
#     if obj["impressions_byappid"]:
#         err_count = config_index(doc, obj["impressions_byappid"], 'impressions_byappid', err_count)
#         if DEBUG:
#             index_counting["imaid_count"] += 1
#     # Index accounts_byappid to Elasticsearch:
#     if obj["accounts_byappid"]:
#         err_count = config_index(doc, obj["accounts_byappid"], 'accounts_byappid', err_count)
#         if DEBUG:
#             index_counting["accaid_count"] += 1
#     # Index installPerAcc_byappid to Elasticsearch:
#     if obj["installPerAcc_byappid"]:
#         err_count = config_index(doc, obj["installPerAcc_byappid"], 'installperaccount_byappid', err_count)
#         if DEBUG:
#             index_counting["inaid_count"] += 1
#     # Index installsPerCam_byplat to Elasticsearch:
#     if obj["installsPerCam_byplat"]:
#         err_count = config_index(doc, obj["installsPerCam_byplat"], 'installspercampaign_byadplatform', err_count)
#         if DEBUG:
#             index_counting["inpla_count"] += 1
#     # Index installsPerAcc_weekly to Elasticsearch:
#     if obj["installsPerAcc_weekly"]:
#         err_count = config_index(doc, obj["installsPerAcc_weekly"], 'installsperacc_weeklychange', err_count)
#         if DEBUG:
#             index_counting["inacc_count"] += 1
#     # Index installsPerAcc_weekly to Elasticsearch:
#     if obj["RR_bycamtag"]:
#         err_count = config_index(doc, obj["RR_bycamtag"], 'resulterate_bycamtag', err_count)
#         if DEBUG:
#             index_counting["rrcam_count"] += 1
#     if DEBUG:
#         index_counting["err_count"] = err_count
#         if err_count == 0:
#             return True, index_counting
#         else:
#             return False, index_counting
#     else:
#         if err_count == 0:
#             return True,None
#         else:
#             return False,err_count

##################################### FETCH DIMENSION VALUE FROM ES BY DATE ############################################
# Initial 5 necessary variable
# PLATFORM = fetchData('ad_platform', DATE)
# APPID = fetchData('app_id',DATE)
# CAMPAIGNTAG = fetchData('campaign_tag',DATE)
# REGIONS = fetchData('region',DATE)
# APPSERIES = ["NOT (app_id: 1145 OR app_id: 1180 OR app_id: 1233)","app_id: 1145 OR app_id: 1180 OR app_id: 1233"]
#
# if DEBUG:
#     print("=" * 80)
#     print("Platform = ", PLATFORM)
#     print("=" * 80)
#     print("app id = ", APPID)
#     print(len(APPID))
#     print("=" * 80)
#     print("app series = ", APPSERIES)
#     print("=" * 80)
#     print("campaign tag = ", CAMPAIGNTAG)
#     print("=" * 80)
#     print("region = ", REGIONS)
#     print(len(REGIONS))
############################################### MAIN FUNCTION ############################################
def index_by_day(DATE, PLATFORM, APPID, APPSERIES, CAMPAIGNTAG, REGIONS):

    time1 = datetime.datetime.utcnow()
    if DEBUG:
        i = 1
    err_count = 0
    for pla in PLATFORM:
        for app in APPID:
            for series in APPSERIES:
                for cam in CAMPAIGNTAG:
                    for reg in REGIONS:
                        # Pass dimensions' information to functions:
                        FLAG = {
                                "id": app,
                                "platform": pla,
                                "series": series,
                                "campaign_tag": cam,
                                "region": reg
                            }

                        line = ""
                        # Query Elasticsearch for result:
                        EXIST, installs,clicks, impressions, cost, CTR,CVR,RR,CPM,CPA, imprePerCam, imprePerAcc = index(DATE, FLAG)
                        if EXIST:
                            SIGNALID, onlinegid_byAppID, total_impressions, accounts_appID, installsPerAcc_byAppID  = indexGroupByAppid(DATE,FLAG)
                            SIGNALPLA, installsPerCam_byPlat, installsPerAcc_weeklychange = indexGroupByPlat(DATE, FLAG)
                            SIGNALCAM, RR_byCamTag = indexGroupByCamTag(DATE,FLAG)
                            # Build an obj that pass all required value to bulk index function
                            obj = {
                                "installs": installs,
                                "clicks": clicks,
                                "impressions": impressions,
                                "cost": cost,
                                "CTR": CTR,
                                "CVR": CVR,
                                "ResultRate": RR,
                                "CPM": CPM,
                                "CPA": CPA,
                                "impressions_per_campaign": imprePerCam,
                                "impressions_per_account": imprePerAcc,
                                "onlinegid_byappID": onlinegid_byAppID,
                                "impressions_byappid":total_impressions,
                                "accounts_byappid": accounts_appID,
                                "installperaccount_byappid": installsPerAcc_byAppID,
                                "installspercampaign_byadplatform": installsPerCam_byPlat,
                                "installsperacc_weeklychange": installsPerAcc_weeklychange,
                                "resulterate_bycamtag": RR_byCamTag
                            }
                            if DEBUG_TOFILE:
                                if series == "NOT (app_id: 1145 OR app_id: 1180 OR app_id: 1233)":
                                    ser = "TopBuzz"
                                else:
                                    ser = "IES"
                                line = "id," + app + ",platform," + pla + ",series," + ser + ",campaigntag," + cam + ",region," + reg + ","
                            if DEBUG:
                                if app == "1145" and pla == "facebook_agency" and series == "app_id: 1145 OR app_id: 1180 OR app_id: 1233" and cam == "NULL" and reg == "BR":
                                    print(json.dumps(obj))
                                if app == "1233" and pla == "facebook_agency" and series == "app_id: 1145 OR app_id: 1180 OR app_id: 1233" and cam == "NULL" and reg == "TR":
                                    print(json.dumps(obj))
                                if app == "1180" and pla == "facebook_automatic" and series == "app_id: 1145 OR app_id: 1180 OR app_id: 1233" and cam == "automatic_copy" and reg == "ID":
                                    print(json.dumps(obj))
                                if app == "1193" and pla == "facebook_automatic" and series == "NOT (app_id: 1145 OR app_id: 1180 OR app_id: 1233)" and cam == "automatic_new" and reg == "BR":
                                    print(json.dumps(obj))
                            if DEBUG_TOFILE:
                                if EXIST:
                                    for key, value in obj.items():
                                        line +=  str(key) + "," + str(value) + ","
                            ###################### INDEX TO ELASTICSEARCH #######################,
                            # Build doc:
                            if series == "NOT (app_id: 1145 OR app_id: 1180 OR app_id: 1233)":
                                ser = "TopBuzz"
                            else:
                                ser = "IES"
                            #### INDEX clicks #####
                            doc = {
                                'id': "",
                                'value': installs,
                                'date_time': DATE,
                                'ad_platform': pla,
                                'app_series': ser,
                                'app_id': app,
                                'region': reg,
                                'campaign_tag': cam,
                                'metric_type': 'installs'
                            }
                            #USE CONSTANT TO CORRECT RESULT:
                            # result, count = indexToElastic(obj,doc)
                            fault_tolerance = 20
                            fault = 0
                            result = es_add_bulk(doc, obj, THREAD_COUNT, QUEUE_SIZE)
                            while not result and fault < fault_tolerance:
                                fault += 1
                                result = es_add_bulk(doc, obj , THREAD_COUNT, QUEUE_SIZE)
                                print(doc)
                                print(result)
                                print("attempt to update", fault)
                            # # debug method for one by one indexing
                            # if not result:
                            #     print(count)
                            #     if DEBUG:
                            #         err_count += count["err_count"]
                            #     else:
                            #         err_count += count
                            ###################### PRINT THE PROCESS #######################
                            if DEBUG:
                                #print("result", json.loads(obj))
                                if int(i/100) == (i/100):
                                    print("=" * 20, i, " complete.", "=" * 20)
                                    localtime = time.asctime(time.localtime(time.time()))
                                    print(localtime)
                                i += 1
                            if DEBUG_TOFILE:
                                line = line + ",err," + str(err_count)
                                #template = "./../data/debug_" + DATE + ".csv"
                                # If runing overall:
                                template = "data/debug_" + DATE + ".csv"
                                with open(template, "a") as file:
                                    file.write(line)
                                    file.write('\n')
                            time.sleep(0.01)
    # if DEBUG and DEBUG_TOFILE:
    # old
    #     for key, value in count.items():
    #         line += "," + str(key) + "," + str(value)
    #     with open(template, "a") as file:
    #         file.write(line)
    #         file.write('\n')
    time2 = datetime.datetime.utcnow()
    totalseconds = (time2 - time1).total_seconds()
    print("total seconds costs: ", totalseconds)
####################### MAIN FUNCTION ####################
# User Customize:
# FROM_DATE = "2018-05-31"
# TO_DATE = "2018-06-01"
# FROM_DATE = datetime.date(2018,6,24)
# TO_DATE = datetime.date(2018,7,2)

# daterange: inclue FROM_DATE and TO_DATE
def daterange(FROM_DATE, TO_DATE):
    for n in range(int((TO_DATE - FROM_DATE).days) + 1):
        yield FROM_DATE + datetime.timedelta(n)

# # RUN THE LOOP:
# for single_date in daterange(FROM_DATE, TO_DATE):
#     DATE = single_date.strftime("%Y-%m-%d")
#     if DEBUG:
#         print("DATE, ", DATE)
#     # Update 5 necessary variable
#     PLATFORM = fetchData('ad_platform', DATE)
#     APPID = fetchData('app_id',DATE)
#     CAMPAIGNTAG = fetchData('campaign_tag',DATE)
#     REGIONS = fetchData('region',DATE)
#     APPSERIES = ["NOT (app_id: 1145 OR app_id: 1180 OR app_id: 1233)","app_id: 1145 OR app_id: 1180 OR app_id: 1233"]
#     if DEBUG:
#         print("=" * 80)
#         print("Platform = ", PLATFORM)
#         print("=" * 80)
#         print("app id = ", APPID)
#         print(len(APPID))
#         print("=" * 80)
#         print("app series = ", APPSERIES)
#         print("=" * 80)
#         print("campaign tag = ", CAMPAIGNTAG)
#         print("=" * 80)
#         print("region = ", REGIONS)
#         print(len(REGIONS))
#     index_by_day(DATE)



















