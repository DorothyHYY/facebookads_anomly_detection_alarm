import unittest
from unittest import mock
import BulkElasticsearch.bulk_update as bulk_update
import elasticsearch


class MyTestCase(unittest.TestCase):
    def setUp(self):
        self.doc = {
                    "date_time": "2018-06-12",
                    "ad_platform": "facebook",
                    "app_series": "IES",
                    "app_id": "1180",
                    "region": "US",
                    "campaign_tag": "NULL"
                    }
        self.obj_dict = {
                        "install": 10000,
                        "CTR": 8.2,
                        "CVR": 0.08,
                        "ResultRate": 1.2
                    }
        self.score_list = [
            {
                "date_time": "2018-06-12",
                "ad_platform": "facebook",
                "app_series": "IES",
                "app_id": "1180",
                "region": "US",
                "campaign_tag": "NULL",
                "metric_type": "install",
                "anomly_score": 0.08,
                "label": "True"
            }]



    def test_encode_monitor(self):
        """encode_monitor with legal input"""
        idx_list = []
        doc_lst = []
        for idx, obj in bulk_update.encode_monitor(self.doc, self.obj_dict):
            keywords = []
            value = []
            for key, val in obj.items():
                keywords.append(key)
                value.append(val)
            self.assertEqual(len(keywords), 8)
            id = self.doc["date_time"] + "_" + self.doc["ad_platform"] + "_" + self.doc["app_series"] + "_" + self.doc["app_id"] + "_" + self.doc["region"] + "_" + self.doc["campaign_tag"] + "_" + obj["metric_type"]
            self.assertEqual(idx, id)
            if obj["metric_type"] == "install":
                self.assertEqual(obj["value"], self.obj_dict["install"])
            idx_list.append(idx)
            doc_lst.append(doc_lst)
        self.assertEqual(len(idx_list), len(list(self.obj_dict)))
        print('shortDescription(): ', self.shortDescription())


    def test_illegal_input_encode(self):
        """encode_monitor with illegal input"""
        doc = {
            "ad_platform": "facebook",
            "app_series": "IES",
            "app_id": "1180",
            "region": "US",
            "campaign_tag": "NULL"
        }
        with self.assertRaises(Exception):
            list(bulk_update.encode_monitor(doc, self.obj_dict))
        print('shortDescription(): ', self.shortDescription())


    def test_add_anomly(self):
        """add_anomly with legal input"""
        idx_list = []
        doc_lst = []
        for idx, obj in bulk_update.add_anomaly(self.score_list):
            keywords = []
            value = []
            for key, val in obj.items():
                keywords.append(key)
                value.append(val)
            self.assertEqual(len(keywords), 2)

            id = self.score_list[0]["date_time"] + "_" + self.score_list[0]["ad_platform"] + "_" + self.score_list[0]["app_series"] + "_" + self.score_list[0]["app_id"] + "_" + self.score_list[0]["region"] + "_" + self.score_list[0]["campaign_tag"] + "_" + self.score_list[0]["metric_type"]
            self.assertEqual(idx, id)
            if self.score_list[0]["metric_type"] == "install":
                self.assertEqual(obj["anomly_score"], self.score_list[0]["anomly_score"])
            idx_list.append(idx)
            doc_lst.append(obj)
        self.assertEqual(len(idx_list), len(list(self.score_list)))
        print('shortDescription(): ', self.shortDescription())


    def test_illegal_input_anomly(self):
        """add_anomly with illegal input"""
        doc = {
                "ad_platform": "facebook",
                "app_series": "IES",
                "app_id": "1180",
                "region": "US",
                "campaign_tag": "NULL",
                "metric_type": "install",
                "anomly_score": 0.08,
                "label": "True"
        }
        with self.assertRaises(Exception):
            list(bulk_update.add_anomaly([doc]))
        print('shortDescription(): ', self.shortDescription())

    @mock.patch('elasticsearch.helpers.bulk')
    def test_es_add_bulk(self, mock_bulk):
        """test es_add_bulk with legal input"""
        bulk_update.es_add_bulk(self.doc, self.obj_dict)
        mock_bulk.assert_called()
        print('shortDescription(): ', self.shortDescription())

    @mock.patch('elasticsearch.helpers.bulk')
    def test_illegal_add_bulk_empty(self, mock_bulk):
        """test es_add_bulk with illegal input"""
        res = bulk_update.es_add_bulk({}, {})
        self.assertFalse(res)
        mock_bulk.assert_not_called()
        print('shortDescription(): ', self.shortDescription())

    @mock.patch('elasticsearch.helpers.bulk')
    def test_illegal_add_bulk(self, mock_bulk):
        """test es_add_bulk with exception"""
        mock_bulk.side_effect = elasticsearch.exceptions.TransportError(404, "wrong", None)
        res = bulk_update.es_add_bulk(self.doc, self.obj_dict)
        self.assertFalse(res)
        print('shortDescription(): ', self.shortDescription())


    @mock.patch('elasticsearch.helpers.parallel_bulk')
    def test_es_update_bulk(self, mock_bulk):
        """test es_update_bulk with legal input"""
        mock_bulk.return_value=[[True, True] for _ in range(len(self.score_list))]
        res = bulk_update.es_update_bulk(self.score_list, 4, 4)
        mock_bulk.assert_called_once()
        self.assertTrue(res)
        print('shortDescription(): ', self.shortDescription())

    @mock.patch('elasticsearch.helpers.parallel_bulk')
    def test_es_update_bulk_fail(self, mock_bulk):
        """test es_update_bulk, expect es fails first then success"""
        bulk_result =  [[True, True]] * 2
        bulk_result[0] = [True, False]
        mock_bulk.return_value = (i for i in bulk_result)
        res = bulk_update.es_update_bulk(self.score_list, 4, 4)
        self.assertTrue(res)
        print('shortDescription(): ', self.shortDescription())

    @mock.patch('elasticsearch.helpers.parallel_bulk')
    def test_es_update_bulk_alwaysfail(self, mock_bulk):
        """test es_update_bulk with, expect to be failed"""
        mock_bulk.return_value = [[True, False] for _ in range(len(self.score_list))]
        res = bulk_update.es_update_bulk(self.score_list, 4, 4)
        self.assertEqual(mock_bulk.call_count, 21)
        self.assertFalse(res)
        print('shortDescription(): ', self.shortDescription())

    @mock.patch('elasticsearch.helpers.parallel_bulk')
    def test_illegal_update_bulk_empty(self, mock_bulk):
        """test es_update_bulk with empty input ; with none list-type input ; with thread count as 0 """
        res = bulk_update.es_update_bulk([], 1, 1)
        mock_bulk.assert_not_called()
        self.assertFalse(res)
        with self.assertRaises(Exception):
            bulk_update.es_update_bulk({}, 1, 1)
        with self.assertRaises(Exception):
            bulk_update.es_update_bulk(self.score_list, 0, 1)
        print('shortDescription(): ', self.shortDescription())

    @mock.patch('elasticsearch.helpers.parallel_bulk')
    def test_exception_update_bulk_exception(self, mock_bulk):
        """test es_update_bulk with exception """
        mock_bulk.side_effect = elasticsearch.exceptions.TransportError(404, "wrong", None)
        res = bulk_update.es_update_bulk(self.score_list, 1, 1)
        self.assertFalse(res)
        print('shortDescription(): ', self.shortDescription())
