from elasticsearch import Elasticsearch as ES
from elasticsearch import helpers
# Initiate
es = ES(['10.11.40.134', '10.11.40.135', '10.11.40.194', '10.11.40.195'], port=9200)
THREAD_COUNT = 4
QUEUE_SIZE = 4

def encode_monitor(doc, obj_dic):
    doc_template = ["date_time", "ad_platform", "app_series", "app_id", "region", "campaign_tag"]
    for item in doc_template:
        if item not in doc:
            raise Exception("Requirements are not satisfied, ", item, "is lost.")
    # obj_dic = ["installs", "clicks", "impressions", "cost", "CTR", "CVR", "ResultRate", "CPM", "CPA", "impressions_per_campaign", "impressions_per_account", "onlinegid_byappID", "impressions_byappid", "accounts_byappid", "installperaccount_byappid", "installspercampaign_byadplatform", "installsperacc_weeklychange", "resulterate_bycamtag"]
    for keywords, value in obj_dic.items():
        idx = doc['date_time'] + "_" + doc['ad_platform'] + "_" + doc['app_series'] + "_" + doc['app_id'] + "_" + doc['region'] + "_" + doc['campaign_tag'] + "_" + keywords
        es_fields_keys = ('value', 'date_time', 'ad_platform', 'app_series', 'app_id', 'region', 'campaign_tag', 'metric_type')
        es_fields_vals = (value, doc['date_time'], doc['ad_platform'], doc['app_series'], doc['app_id'], doc['region'], doc['campaign_tag'], keywords)
        # Return a dict holding values from each doc:
        es_monitor_d = dict(zip(es_fields_keys, es_fields_vals))
        # Return the row on each iteration
        yield idx, es_monitor_d


def es_add_bulk(doc, obj_list):
    es = ES()
    # This is for a generator.
    if doc == {} or obj_list == {}:
        return False
    action = ({
            "_index": "monitor",
            "_type" : "doc",
            "_id"   : idx,
            "_source": es_monitor_d,
         } for idx, es_monitor_d in encode_monitor(doc, obj_list))
    try:
        helpers.bulk(es, action)
        return True
    except Exception as err:
        print(err)
        return False


def add_anomaly(obj_list):
    doc_template = ["date_time", "ad_platform", "app_series", "app_id", "region", "campaign_tag", "metric_type"]
    # obj_dic = ["installs", "clicks", "impressions", "cost", "CTR", "CVR", "ResultRate", "CPM", "CPA", "impressions_per_campaign", "impressions_per_account", "onlinegid_byappID", "impressions_byappid", "accounts_byappid", "installperaccount_byappid", "installspercampaign_byadplatform", "installsperacc_weeklychange", "resulterate_bycamtag"]
    for doc in obj_list:
        for item in doc_template:
            if item not in doc:
                raise Exception("Requirements are not satisfied, ", item, "is lost.")
        idx = doc['date_time'] + "_" + doc['ad_platform'] + "_" + doc['app_series'] + "_" + doc['app_id'] + "_" + doc['region'] + "_" + doc['campaign_tag'] + "_" + doc["metric_type"]
        es_fields_keys = ("anomly_score", "label")
        es_fields_vals = (doc["anomly_score"], doc["label"])
        # Return a dict holding values from each doc:
        es_monitor_update = dict(zip(es_fields_keys, es_fields_vals))
        # Return the row on each iteration
        yield idx, es_monitor_update


def es_update_bulk(obj_list, thread_count, queue_size):
    # instantiate es client, connects to localhost:9200 by default
    es = ES(['10.11.40.134', '10.11.40.135', '10.11.40.194', '10.11.40.195'], port=9200)
    if thread_count == 0:
        raise Exception("thread count at least to be 1")
    if type(obj_list) != type([]):
        raise Exception("the first parameter must be list type")
    # This is for a generator.
    if len(list(obj_list)) == 0:
        return False
    action = ({
            "_op_type": 'update',
            "_index": "monitor",
            "_type" : "doc",
            "_id"   : idx,
            "doc": es_monitor_update,
         } for idx, es_monitor_update in add_anomaly(obj_list))
    try:
        cur_num = 0
        fault_tolerance = 20
        fault = 0
        for ok, result in helpers.parallel_bulk(es, action, thread_count=thread_count, queue_size=queue_size):
            # If fails and number of failures is within fault_tolerance, retry:
            while not result and fault < fault_tolerance:
                fault += 1
                print("fail", obj_list[cur_num])
                action = ({
                    "_op_type": 'update',
                    "_index": "monitor",
                    "_type": "doc",
                    "_id": idx,
                    "doc": es_monitor_update,
                } for idx, es_monitor_update in add_anomaly([obj_list[cur_num]]))
                r = [res for _, res in helpers.parallel_bulk(es, action, thread_count=thread_count, queue_size=queue_size, request_timeout=30)]
                result = r[0]
            if fault >= fault_tolerance and not result:
                return False
            cur_num += 1
        return True
    except Exception as err:
        print(err)
        return False


#
#
# doc = {
#             "date_time": "2018-06-12",
#             "ad_platform": "facebook",
#             "app_series": "IES",
#             "app_id": "1180",
#             "region": "US",
#             "campaign_tag": "NULL"
#             }
# obj_dict = {
#                 "install": 8888888,
#                 "CTR": 0.002,
#                 "CVR": 10,
#                 "ResultRate": 10
#             }
#
# score_dict = {
#                 "anomaly_score": 0.33,
#                 "label": "True"
#             }


# def queryES():
#     try:
#         es = ES()
#         es.indices.refresh(index="monitor")
#         res = es.search(index="monitor", body={"query": {"match_all": {}}})
#         print("Got %d Hits:" % res['hits']['total'])
#         print(res)
#         return res
#     except Exception as err:
#         print(err)
#         return False

# Another search method that might speed up the search.
# def getES(obj):
#     """
#     Only return index, type, id.. not source
#     """
#     try:
#         time1 = datetime.datetime.utcnow()
#         res = es.get(
#             index="monitor",
#             doc_type="doc",
#             id=obj['date_time'] + "_" + obj['ad_platform'] + "_" + obj['app_series'] + "_" + obj['app_id'] + "_" + obj['region'] + "_" + obj['campaign_tag'] + "_" + obj["metric_type"]
#         )
#         exist = list(res)
#         print(res)
#         time2 = datetime.datetime.utcnow()
#         totalseconds = (time2 - time1).total_seconds()
#         print("total seconds costs: ", totalseconds)
#         if len(exist) != 0:
#             return True
#         else:
#             return False
#     except TransportError as err:
#         print(err)
#         print(err.info["found"])
#         time2 = datetime.datetime.utcnow()
#         totalseconds = (time2 - time1).total_seconds()
#         print("total seconds costs: ", totalseconds)
#         if "found" in err.info and err.info["found"] == False:
#             return False
#         else:
#             raise err


# Another search method that might speed up the search.
# def scanES(obj):
#     try:
#         res = helpers.scan(
#             ES(['10.11.40.134', '10.11.40.135', '10.11.40.194', '10.11.40.195'], port=9200),
#             index="monitor",
#             doc_type="doc",
#             query={
#                   "query":{
#                     "bool":{
#                       "must":[{
#                         "match": {
#                           "ad_platform": obj["ad_platform"]
#                         }
#                       },
#                       {
#                         "match": {
#                           "app_id": obj["app_id"]
#                         }
#                       },{
#                         "match": {
#                           "campaign_tag": obj["campaign_tag"]
#                         }
#                       },{
#                         "match": {
#                           "region": obj["region"]
#                         }
#                       },
#                       {
#                           "match": {
#                               "app_series": obj["app_series"]
#                           }
#                       },
#                       {
#                         "match": {
#                           "date_time": obj["date_time"]
#                         }
#                       }
#                       ]
#                     }
#                   }
#                 }
#         )
#         exist = list(res)
#         metric_type = []
#         for type in exist:
#             metric_type.append(type["_source"]["metric_type"])
#         if len(exist) != 0 and len(metric_type) != 0:
#             return metric_type
#         else:
#             return False
#     except elasticsearch.exceptions as err:
#         raise err
