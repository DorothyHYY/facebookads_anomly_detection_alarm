import numpy as np
import math
import operator
import datetime
from elasticsearch import Elasticsearch
from concurrent.futures import ThreadPoolExecutor, as_completed
import elasticsearch
from elasticsearch import helpers
from copy import copy
import os
import yaml
import itertools

DEBUG = False
DEBUG_PRINT = True
########################################################################################################################
###################################################### UserCostomize ###################################################
########################################################################################################################
FROM_DATE = datetime.date(2018,5,29)
TO_DATE = datetime.date(2018,6,21)
# Setting configuration
config = os.path.join(os.path.dirname(__file__), "config.yaml")
with open(config, "r") as f:
    settings = yaml.load(f)


ES_IP = settings['elasticsearch']['ip']
ES_PORT = settings['elasticsearch']['port']
# instantiate es client, connects to localhost:9200 by default
es = Elasticsearch(ES_IP, port=ES_PORT)
# Setting config for helpers.parallel_bulk:
THREAD_COUNT = settings['anomly_detection']['THREAD_COUNT']
QUEUE_SIZE = settings['anomly_detection']['QUEUE_SIZE']
# # Define the window's size
# ## If the window size of each word in SAX is too short, then the shorter subsequence represented by each SAX word may not be anomalous and we have too many false negatives.
# ## If the window size is too long, then the number of SAX words obtained in each subsequence(of length w) is less, and their frequencies may be low even id the sequence is not anomalous.
WINDOW_SIZE = settings['anomly_detection']['WINDOW_SIZE']
PAA_SEGMENTS = settings['anomly_detection']['PAA_SEGMENTS']
SAX_WORDS = settings['anomly_detection']['SAX_WORDS']
KNN_SIZE = settings['anomly_detection']['KNN_SIZE']
# strict series length to 14
SERIES_LENGTH = settings['anomly_detection']['SERIES_LENGTH']
NEW_TOLERANCE = settings['anomly_detection']['NEW_TOLERANCE']
########################################################################################################################
####################################################### Initialize #####################################################
########################################################################################################################
# Normalize series
def norm_raw(x):
    x = np.array(x)
    x_mean = np.mean(x)
    x_sigma = np.std(x)
    return [(i-x_mean) / x_sigma if x_sigma != 0 else 0 for i in x]


# daterange: inclue FROM_DATE and TO_DATE
def daterange(FROM_DATE, TO_DATE):
    for n in range(int((TO_DATE - FROM_DATE).days) + 1):
        yield FROM_DATE + datetime.timedelta(n)


# Get time series:
def get_raw(DATE,obj):
    try:
        result = es.search(
            index='monitor',
            body={
                  "query":{
                    "bool":{
                      "must":[{
                        "match": {
                          "ad_platform": obj["ad_platform"]
                        }
                      },
                      {
                        "match": {
                          "app_id": obj["app_id"]
                        }
                      },{
                        "match": {
                          "campaign_tag": obj["campaign_tag"]
                        }
                      },{
                        "match": {
                          "region": obj["region"]
                        }
                      },{
                        "match": {
                          "metric_type": obj["metric_type"]
                        }
                      },
                      {
                        "match": {
                          "date_time": DATE
                        }
                      }
                      ]
                    }
                  }
                }
        )
        if result['hits']['total'] != 0:
            return result['hits']['hits'][0]['_source']['value']
        else:
            return 0
    except Exception as err:
        raise err

# Use ThreadPoolExecutor to deal with concurrency, speed up the process:
def get_series(DATE, obj):
    work_num = 14
    with ThreadPoolExecutor(max_workers=work_num) as exector:
        future_list = []
        for date in DATE:
        # Use submit to hand funcs into thread pool, and return future object. (syncro)
            future = exector.submit(get_raw, date, obj)
            future_list.append(future)
        result = []
        # Pass a Future iterator to as_completed; yield Future after finishing using future object.
        for future in future_list:
            # Fetch result by callig result()
            res = future.result()
            result.append(res)
    return result

# Single Process method to fetch series, this one is abandoned now.
# Get time series:
# def get_raw(DATE,obj):
#     result = es.search(
#         index='monitor',
#         body={
#               "query":{
#                 "bool":{
#                   "must":[{
#                     "match": {
#                       "ad_platform": obj["ad_platform"]
#                     }
#                   },
#                   {
#                     "match": {
#                       "app_id": obj["app_id"]
#                     }
#                   },{
#                     "match": {
#                       "campaign_tag": obj["campaign_tag"]
#                     }
#                   },{
#                     "match": {
#                       "region": obj["region"]
#                     }
#                   },{
#                     "match": {
#                       "metric_type": obj["metric_type"]
#                     }
#                   },
#                   {
#                     "match": {
#                       "date_time": DATE
#                     }
#                   }
#                   ]
#                 }
#               }
#             }
#     )
#     return result
# def get_series(DATE, series, obj):
#     result = get_raw(DATE, obj)
#     if DEBUG:
#         print(result)
#     series.append(copy(fetch_result(result)))
#     return series
# def fetch_result(re):
#     hits = re['hits']['total']
#     if hits == 0:
#         return 0
#     else:
#         result = re['hits']['hits'][0]["_source"]["value"]
#         return result


########################################################################################################################
####################################### Calculate distance between two sub sequence ####################################
########################################################################################################################
# DIFFSTD:
def DIFFSTD(x1,x2):
    """
    Calculate the variance of differences between two sereis
    """
    n = len(x1)
    ssq = 0
    for i in range(n):
        ssq = ssq + (x1[i] - x2[i])**2
    sqs = 0
    for i in range(n):
        tmp = x1[i] - x2[i]
        if tmp < 0:
            tmp *= -1
        sqs = sqs + tmp
    res = (n * ssq - (sqs**2)) / (n*(n-1)) if n > 1 else 0
    return res


# STREDND:
def STREND(x1,x2):
    """
    Calculate the difference of trend for two time series.
    """
    n = len(x1)
    del1 = [x1[i] - x1[i-1] for i in range(1,n,1)]
    del2 = [x2[i] - x2[i-1] for i in range(1,n,1)]
    s = np.array(del1) * np.array(del2) >= 0
    s = [1 if x == True else 0 for x in s]
    if n > 1:
        res = sum(s)/(n-1)
    else:
        res = sum(s)
    return res


# SAXBAG:
def PAA_trans(series):
    """
    Transform the time sereis to PAA series
    """
    # Remember first normalize
    series_len = len(series)
    if (series_len == PAA_SEGMENTS):
        return np.copy(series)
    else:
        res = np.zeros(PAA_SEGMENTS)
        # n/w is a whole number
        if (series_len % PAA_SEGMENTS == 0):
            inc = series_len // PAA_SEGMENTS
            for i in range(PAA_SEGMENTS):
                series = np.array(series)
                res[i] += sum(series[inc * i:inc * i + inc])
            return res / inc
        # n/w is a fraction
        else:
            # Use a trick, first add multiple times, then devided the multiple value
            for i in range(PAA_SEGMENTS * series_len):
                idx = i // series_len
                pos = i // PAA_SEGMENTS
                np.add.at(res, idx, series[pos])
            # return res / paa_segments / (series_len / paa_segments)
            return res / series_len


def SAX_words(x, sax_words=SAX_WORDS):
    """
    Transform PAA series to SAX words.
    """
    res = []
    if len(x) >= sax_words:
        if sax_words == 3:
            for _ in x:
                res = ["a" if i < -0.84 else "c" if i > 0.43 else "b" for i in x]
        elif sax_words == 4:
            for _ in x:
                res = ["a" if i < -0.67 else "b" if i < 0 else "c" if i < 0.67 else "d" for i in x]
        elif sax_words == 5:
            for _ in x:
                res = ["a" if i < -0.84 else "b" if i < -0.25 else "c" if i < 0.25 else "d" if i < 0.84 else "e" for i
                       in x]
        elif sax_words == 6:
            for _ in x:
                res = [
                    "a" if i < -0.97 else "b" if i < -0.43 else "c" if i < 0 else "d" if i < 0.43 else "e" if i < 0.97 else "f"
                    for i in x]
        elif sax_words == 7:
            for _ in x:
                res = [
                    "a" if i < -1.07 else "b" if i < -0.57 else "c" if i < -0.18 else "d" if i < 0.18 else "e" if i < 0.57 else "f" if i < 1.07 else "g"
                    for i in x]
    else:
        print(len(x))
        raise Exception("There is meaningless to apply SAX alphabet whose length is bigger than series size")
    return res


def comb(sax_words=SAX_WORDS):
    """
    Parameters: SAX_WORDS;
    Return: a dictionary, contains all possible combination of words in alphabets, eg: "abc","abb","acc"...
    """
    keywords = {}
    if sax_words == 3:
        keywords = {''.join(i): 0 for i in itertools.product('abc', repeat=3)}
    elif sax_words == 4:
        keywords = {''.join(i): 0 for i in itertools.product('abcd', repeat=4)}
    elif sax_words == 5:
        keywords = {''.join(i): 0 for i in itertools.product('abcde', repeat=5)}
    elif sax_words == 6:
        keywords = {''.join(i): 0 for i in itertools.product('abcdef', repeat=6)}
    elif sax_words == 7:
        keywords = {''.join(i): 0 for i in itertools.product('abcdefg', repeat=7)}
    return keywords



def SAX_represent(x, dic, sax_words=SAX_WORDS):
    """
    Use dictionray to store the sax words of series.
    """
    if len(x) < sax_words:
        print(len(x))
        raise Exception("Series transformed to sax words only has ", len(x),
                        " size, while to traverse a possible combination of all sax words we need length of ",
                        sax_words, ".")
    x = np.array(x)
    re = ""
    for i in x[:sax_words]:
        re += i
    dic[re] += 1
    if len(x) == sax_words:
        return dic
    else:
        for fla in range(1, len(x) - sax_words + 1, 1):
            re = ""
            for i in x[fla:sax_words + fla]:
                re += i
            dic[re] += 1
        return dic


def conver_str(dic):
    """
    Convert dictionary's value to string.
    """
    res = []
    for key, value in dic.items():
        res.append(copy(str(value)))
    return ''.join(res)


def is_mindist_zero(a, b):
    """
    Compare two series' difference
    """
    if len(a) != len(b):
        return 0
    else:
        dis = 0
        for i in range(len(b)):
            if ord(a[i]) - ord(b[i]) != 0:
                dis += 1
        return dis


def SAXBAG(SUB_SEQUENCE1, SUB_SEQUENCE2):
    words_x1 = SAX_words(PAA_trans(SUB_SEQUENCE1))
    words_x2 = SAX_words(PAA_trans(SUB_SEQUENCE2))
    pattern_x1 = SAX_represent(words_x1, comb())
    pattern_x2 = SAX_represent(words_x2, comb())
    final_x1 = conver_str(pattern_x1)
    final_x2 = conver_str(pattern_x2)
    return is_mindist_zero(final_x1, final_x2)

########################################################################################################################
####################################################### Normalization ##################################################
########################################################################################################################
# Use these normalization methods to normalize three distance measurements, because eg the strend is from 0 to 1, while saxbag is from 0 to l*sqrt(2) where l = # words obtained from the entire series.
def normalization(x):
    """
    Normalization method from Anomaly book.
    """
    x = np.array(x)
    x = np.sort(x)
    n = len(x)
    start = int(0.05*n)
    end = int(0.95*n)
    x_mean = np.mean(x[start:end])
    return [i/x_mean if x_mean != 0 else 0 for i in x]


def zero_scroe_normalization(x):
    """
    Implement the zero_score normaliztion method.
    """
    x = np.array(x)
    x = np.sort(x)
    n = len(x)
    start = int(0.05*n)
    end = int(0.95*n)
    x_mean = np.mean(x[start:end])
    x_sigma = np.std(x[start:end])
    return [(i-x_mean)/x_sigma if x_sigma != 0 else 0 for i in x]


def zero_one_normalization(x):
    """
    Implement the (0,1) normalization.
    """
    x = np.array(x)
    x_min = min(x)
    x_max = max(x)
    return [(i-x_min)/(x_max - x_min) if (x_max - x_min) != 0 else 0 for i in x]

# calculate three distance measurements after normalizing a subsequence.
def get_dis_norm(measurement, SUB_SEQUENCE):
    """
    Get the normalization distance for all three measurements.
    For example , for a series [[1,1,1,1,1],[2,2,2,2,2],[3,3,3,3,3]]:
    we have DIFFSTD[[dist1_2, dist1_3],[dist2_1, dist2_3],[dist3_1, dist3_2]],
    STREND[[dist1_2, dist1_3],[dist2_1, dist2_3],[dist3_1, dist3_2]],
    SAXBAG[[dist1_2, dist1_3],[dist2_1, dist2_3],[dist3_1, dist3_2]],
    And their ranges are diff, so we have to pass them all to a (0,1) normalization,
    So that their ranges should be the same.
    """
    measurement_dis = []
    if (len(SUB_SEQUENCE) < 3):
        print("there is no sense to calculate two items distance and pass it to a 0,1 normalization")
        if (len(SUB_SEQUENCE) == 2):
            return [[measurement(SUB_SEQUENCE[0],SUB_SEQUENCE[-1])], [measurement(SUB_SEQUENCE[-1],SUB_SEQUENCE[0])]]
        else:
            raise Exception("must enter subsequence with more than 1")
    for i in range(len(SUB_SEQUENCE)):
        measurement_sub = []
        for j in range(len(SUB_SEQUENCE)):
            if j != i:
                measurement_sub.append(copy(measurement(SUB_SEQUENCE[i],SUB_SEQUENCE[j])))
        intermediate = zero_one_normalization(measurement_sub)
        measurement_dis.append(copy(intermediate))
    return measurement_dis


########################################################################################################################
############################### Get the Euclidean distance to find neighbourhood #######################################
########################################################################################################################
def average_distance(x1,x2):
    """
    Calculate the Euclidean distance between two subsequences.
    """
    if len(x1) != len(x2):
        raise Exception("Length of two subsequence is not equal")
    else:
        n = len(x1)
        # Use Euclidean distance to calculate the distance between two subsequence
        dis = 0
        for i in range(n):
            dis += (x1[i] - x2[i])**2
        dis = math.sqrt(dis)
        return dis

# Calculate the neighborhood distance for all specific subsequence
def get_averagedis_norm(sub_x):
    """
    Calculate the neighborhood distance for the subsequence.
    """
    dis = []
    if len(sub_x) < 2 or len(sub_x[0]) == 0:
        raise Exception("at least have one item")
    for i in range(len(sub_x)):
        sub_dis = []
        for j in range(len(sub_x)):
            if j != i:
                sub_dis.append(copy(average_distance(sub_x[i],sub_x[j])))
        dis.append(copy(sub_dis))
    return dis


########################################################################################################################
########################################## Calculate the weight ########################################################
########################################################################################################################
def rank(x, ind_q, dis_qi):
    """
    Calculate the rank for a time series.
    A sereis o is in the rank if dis(q, o) < dis(q, i).
    """
    dis = []
    for i in range(len(x)):
        dis.append(copy(average_distance(x[ind_q], x[i])))
    dis = np.sort(dis)
    res = 0
    for i in dis:
        if i < dis_qi:
            res += 1
    return res

def outlierness(dis,x):
    """
    Get the outlierness of the subsequence x.
    """
    build_list = [[dis[i], x[i]] for i in range(len(dis))]
    build_list.sort(key=lambda x: x[0])
    dis = [i[0] for i in build_list]
    sequence = [i[1] for i in build_list]
    dis = np.array(dis)
    neighbor_dis = dis[:KNN_SIZE]
    o = 0
    for i in range(len(neighbor_dis)):
        o += rank(sequence, i, neighbor_dis[i])
    return o / len(neighbor_dis)

def weight(dis, x):
    """
    Return the weight of the subsequence x.
    """
    dis_cal = np.array(np.sort(dis))
    neighbor_dis = dis_cal[:KNN_SIZE]
    avg = np.sum(neighbor_dis) / KNN_SIZE
    return outlierness(dis, x) * avg

########################################################################################################################
####################################### Calculate distance from neighborhood ###########################################
########################################################################################################################
def build_dic(dis):
    """
    Build a dictionary whose key is the index position, value is the eculidean distance
    This is easy for us to find the subsequence after we sorting the distance to get the neighbourhood.
    """
    dic = {}
    for i in range(len(dis)):
        dic[i] = dis[i]
    return sorted(dic.items(), key=operator.itemgetter(1))

def avgdis_neigh(measurement, neighbor_dic):
    """
    Get the average distance to the  k nearest neighbords of the ith time series.
    """
    ind = []
    for key,_ in neighbor_dic:
        if len(ind) < KNN_SIZE:
            ind.append(copy(key))
    avgdis = 0
    for i in range(len(ind)):
        avgdis += measurement[i]
    return avgdis / len(ind)

def dis_to_nei(eucdis_ij, measurement_dis):
    """
    Parameters: Euclidean distance between ith and other subsequences to find the neighbourhoods.
    Then using s,t,f distance to calculate the average distance between ith and its neighbourhoods.
    """
    res = build_dic(eucdis_ij)
    avgdis = avgdis_neigh(measurement_dis, res)
    return avgdis

########################################################################################################################
################################## Calculate the abnormaly score for all subsequences ##################################
########################################################################################################################
# calculate th score for xn-7..xn:
def anom_score(eucdis_ij, diffstd, strend, saxbag, sub_x):
    """
    Parameters: The Euclidean distance between ith and other subsequences and three distance measurements, and the subsequence.
    Calculate the anomaly score of the subsequence.
    """
    ascore = 0
    wei = weight(eucdis_ij, sub_x)
    ascore += dis_to_nei(eucdis_ij, diffstd)**2 * wei
    ascore += dis_to_nei(eucdis_ij, strend)**2 * wei
    ascore += dis_to_nei(eucdis_ij, saxbag)**2 * wei
    return math.sqrt(ascore)


# Get all subsequence's anaomaly score:
def get_anomaly_score(all_neighbor_distance, diffstd_norm, strend_norm, saxbag_norm, SUB_SEQUENCE):
    ascore = []
    for i in range(len(diffstd_norm)):
        re = anom_score(all_neighbor_distance[i], diffstd_norm[i], strend_norm[i], saxbag_norm[i], SUB_SEQUENCE)
        ascore.append(copy(re))
    return ascore


########################################################################################################################
############################################## Decided whether is abnormal or not ######################################
########################################################################################################################
def isAbnormal(score, score_list):
    """
    According to the context, score cannot be negative, so we do not consider the condition that it smaller than one
    Which is the negative part of |X-\miu| < 3\sigma.
    """
    score_list = np.array(score_list)
    score_list.sort()
    n = len(score_list)
    score_list = score_list[:int(n*0.8)]
    return score > np.mean(score_list) + 3 * np.std(score_list)


def get_label(ascore):
    """
    The index here we calculates is from the end of series.
    So the result here is negative number, for example the last one has index -1.
    """
    re = []
    for i in range(len(ascore)):
        res = isAbnormal(ascore[i], ascore)
        re.append(copy(res))
    return re


########################################################################################################################
############################################# Scanning methon Elasticsearch ############################################
########################################################################################################################
# The speed comparison among these methods:
# scanES < getES < queryES
# This one is abandoned, the speed of it is the lowest.
# def queryES(date, obj):
#     try:
#         doc_template = ["ad_platform", "app_series", "app_id", "region", "campaign_tag"]
#         for item in doc_template:
#             if item not in obj:
#                 raise Exception("Requirements are not satisfied, ", item, "is lost.")
#         es.indices.refresh(index="monitor")
#         res = es.search(
#             index="monitor",
#             body={
#                   "query":{
#                     "bool":{
#                       "must":[{
#                         "match": {
#                           "ad_platform": obj["ad_platform"]
#                         }
#                       },
#                       {
#                         "match": {
#                           "app_id": obj["app_id"]
#                         }
#                       },{
#                         "match": {
#                           "campaign_tag": obj["campaign_tag"]
#                         }
#                       },{
#                         "match": {
#                           "region": obj["region"]
#                         }
#                       },{
#                         "match": {
#                           "metric_type": obj["metric_type"]
#                         }
#                       },
#                       {
#                           "match": {
#                               "app_series": obj["app_series"]
#                           }
#                       },
#                       {
#                         "match": {
#                           "date_time": date
#                         }
#                       }
#                       ]
#                     }
#                   }
#                 }
#         )
#         if res['hits']['total'] != 0:
#             return True
#         else:
#             return False
#     except elasticsearch.exceptions as err:
#         raise Exception(err)

# Another search method that might speed up the search.
# def getES(date, obj):
#     """
#
#     :param date:
#     :param obj:
#     :return:
#     """
#     try:
#         res = es.get(
#             index="monitor",
#             doc_type="doc",
#             id=date + "_" + obj['ad_platform'] + "_" + obj['app_series'] + "_" + obj['app_id'] + "_" + obj['region'] + "_" + obj['campaign_tag'] + "_" + obj["metric_type"]
#         )
#         exist = list(res)
#         if len(exist) != 0:
#             return True
#         else:
#             return False
#     except (Exception, TypeError) as err:
#         if "found" in err.info and err.info["found"] == False:
#             return False
#         else:
#             raise Exception(err)


# Another search method that might speed up the search
def scanES(date, obj):
    try:
        res = helpers.scan(
            es,
            index="monitor",
            doc_type="doc",
            query={
                  "query":{
                    "bool":{
                      "must":[{
                        "match": {
                          "ad_platform": obj["ad_platform"]
                        }
                      },
                      {
                        "match": {
                          "app_id": obj["app_id"]
                        }
                      },{
                        "match": {
                          "campaign_tag": obj["campaign_tag"]
                        }
                      },{
                        "match": {
                          "region": obj["region"]
                        }
                      },
                      {
                          "match": {
                              "app_series": obj["app_series"]
                          }
                      },
                      {
                        "match": {
                          "date_time": date
                        }
                      }
                      ]
                    }
                  }
                }
        )
        exist = list(res)
        metric_type = []
        for type in exist:
            metric_type.append(copy(type["_source"]["metric_type"]))
        if len(exist) != 0 and len(metric_type) != 0:
            return metric_type
        else:
            return False
    except (elasticsearch.exceptions.TransportError, Exception) as err:
        raise Exception(err)

########################################################################################################################
############################################ Fetch 5 dimensions content and update by DATE ##########################################
########################################################################################################################

def get_list(result):
    if DEBUG:
        print(result)
        print(type(result))
    res = []
    for item in result['aggregations']["date"]['type']['buckets']:
        if DEBUG:
            print("item")
        res.append(copy(item["key"]))
    return res

def fetchData_monitor(var, DATE):
    result = es.search(
        index='monitor',
        body={
              "size":0,
              "aggs": {
                  "date": {
                    "filter": {"term": {"date_time": DATE}},
                    "aggs": {
                      "type": {
                        "terms": {"field": var+".keyword", "size" : 500}
                      }
                    }
                  }
                }
            }
    )
    res = get_list(result)
    return res

########################################################################################################################
####################################################### calculate label after dive into dimensions Function ##################################################
########################################################################################################################
def calculate_and_label(obj, FROM_DATE, TO_DATE):
    DOC_TEMPLATE = {
        'ad_platform': obj["ad_platform"],
        'app_series': obj["app_series"],
        'app_id': obj["app_id"],
        'region': obj["region"],
        'campaign_tag': obj["campaign_tag"],
        'metric_type': obj["metric_type"]
    }

    # Get a date list:
    # eg. DATE = ["2018-05-16", "2018-05-17", ... ,"2018-06-15","2018-06-16"]
    DATE = []
    for single_date in daterange(FROM_DATE, TO_DATE):
        date_tmp = single_date.strftime("%Y-%m-%d")
        DATE.append(copy(date_tmp))
    if DEBUG:
        print("DATE", DATE)
    DATE = np.array(DATE)
    if len(DATE) < SERIES_LENGTH:
        raise Exception("date list's length is smaller than request series_length", SERIES_LENGTH)
    DATE = DATE[(SERIES_LENGTH*-1):]
    # Get a series list:
    SERIES_raw = get_series(DATE, DOC_TEMPLATE)
    SERIES = SERIES_raw
    # Give a tolerance to newly added document, for those series we do not apply anomly detection.
    first_non_zero_ind = next((i for i, x in enumerate(SERIES) if x), None)
    if first_non_zero_ind != None and first_non_zero_ind > len(SERIES)-NEW_TOLERANCE:
        return 0, "False", SERIES_raw[-1]
    if obj["ad_platform"] == "facebook" and obj["app_series"] == "IES" and obj["app_id"] == "1145" and obj["campaign_tag"] == "NULL" and obj["region"] == "BR" and obj["metric_type"] == "CVR":
        print("series: ", SERIES)
        print("date: ", DATE)
    if DEBUG:
        print("series", SERIES)
    if DEBUG:
        print(DATE)
    SERIES = np.array(SERIES)
    SERIES = SERIES[(SERIES_LENGTH*-1):]
    SERIES = norm_raw(SERIES)
    SERIES = np.array(SERIES)

    # Fetching subsequence using window slicing method.
    SUB_SEQUENCE = [SERIES[i:i+WINDOW_SIZE] for i in range(len(SERIES)-WINDOW_SIZE+1)]

    # Get three distance measurements after normalizing
    strend_norm = get_dis_norm(STREND, SUB_SEQUENCE)
    diffstd_norm = get_dis_norm(DIFFSTD, SUB_SEQUENCE)
    saxbag_norm = get_dis_norm(SAXBAG, SUB_SEQUENCE)

    # Get all subsequence's neighborhood distance
    all_neighbor_distance = get_averagedis_norm(SUB_SEQUENCE)

    # Calculate the anormaly score for the subsequence
    ascore = get_anomaly_score(all_neighbor_distance, diffstd_norm, strend_norm, saxbag_norm, SUB_SEQUENCE)
    if obj["ad_platform"] == "facebook" and obj["app_series"] == "IES" and obj["app_id"] == "1145" and obj["campaign_tag"] == "NULL" and obj["region"] == "BR" and obj["metric_type"] == "CVR":
        print("diffstd_norm: ", diffstd_norm)
        print("strend_norm: ", strend_norm)
        print("saxbag_norm: ", saxbag_norm)
        print("all_neighbor_distance: ", all_neighbor_distance)
        print("SUB_SEQUENCE: ", SUB_SEQUENCE)
    if DEBUG:
        print(ascore)
    if obj["ad_platform"] == "facebook" and obj["app_series"] == "IES" and obj["app_id"] == "1145" and obj["campaign_tag"] == "NULL" and obj["region"] == "BR" and obj["metric_type"] == "CVR":
        print("ascore: ", ascore)

    # Label the subsequence
    abnormal = get_label(ascore)
    if DEBUG:
        print(abnormal)

    # Update to elasticsearch:
    id = DATE[-1] + "_" + DOC_TEMPLATE["ad_platform"] + "_" + DOC_TEMPLATE["app_series"] + "_" + DOC_TEMPLATE[
        "app_id"] + "_" + DOC_TEMPLATE["region"] + "_" + DOC_TEMPLATE["campaign_tag"] + "_" + DOC_TEMPLATE[
             "metric_type"]
    if DEBUG:
        print("id", id)

    return ascore[-1], str(abnormal[-1]), SERIES_raw[-1]
