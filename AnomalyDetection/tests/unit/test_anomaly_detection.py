import unittest
import os
from AnomalyDetection.AnormalyDetection import *
from unittest import mock
import numpy as np
import datetime
import elasticsearch




class MyTestCase(unittest.TestCase):
    def setUp(self):
        self.obj = {
                                'ad_platform': "facebook",
                                'app_series': "IES",
                                'app_id': "1180",
                                'region': "US",
                                'campaign_tag': "NULL",
                                'metric_type': "installs"
                            }
        self.FROM_DATE = datetime.date(2018,5,31)
        self.TO_DATE = datetime.date(2018,6,13)
        self.THREAD_COUNT = 6
        self.QUEUE_SIZE = 6
        self.WINDOW_SIZE = 6
        self.PAA_SEGMENTS = 5
        self.SAX_WORDS = 3
        self.KNN_SIZE = 3
        self.SERIES_LENGTH = 14
        self.result_es = {'took': 1, 'timed_out': False, '_shards': {'total': 5, 'successful': 5, 'skipped': 0, 'failed': 0}, 'hits': {'total': 1, 'max_score': 10.718289, 'hits': [{'_index': 'monitor', '_type': 'doc', '_id': '2018-06-07_facebook_agency_IES_1145_BR_NULL_installs', '_score': 10.718289, '_source': {'id': '2018-06-07_facebook_agency_IES_1145_BR_NULL_installs', 'value': 95309.0, 'date_time': '2018-06-07', 'ad_platform': 'facebook_agency', 'app_series': 'IES', 'app_id': '1145', 'region': 'BR', 'campaign_tag': 'NULL', 'metric_type': 'installs'}}]}}
        self.get_es = {'_index': 'monitor', '_type': 'doc', '_id': '2018-06-20_facebook_agency_IES_1145_BR_NULL_installs', '_version': 4, 'found': True, '_source': {'value': 7489.0, 'date_time': '2018-06-20', 'ad_platform': 'facebook_agency', 'app_series': 'IES', 'app_id': '1145', 'region': 'BR', 'campaign_tag': 'NULL', 'metric_type': 'installs', 'anomly_score': 4.615879311906664, 'label': 'True'}}
        self.scan_es = [{'_index': 'monitor', '_type': 'doc', '_id': '2018-06-15_facebook_IES_1145_BR_NULL_onlinegid_byappID', '_score': None, '_source': {'id': '2018-06-15_facebook_IES_1145_BR_NULL_onlinegid_byappID', 'value': 1, 'date_time': '2018-06-15', 'ad_platform': 'facebook', 'app_series': 'IES', 'app_id': '1145', 'region': 'BR', 'campaign_tag': 'NULL', 'metric_type': 'onlinegid_byappID', 'anomly_score': 0.0, 'label': 'False'}, 'sort': [10651]}, {'_index': 'monitor', '_type': 'doc', '_id': '2018-06-15_facebook_IES_1145_BR_NULL_impressions_per_campaign', '_score': None, '_source': {'id': '2018-06-15_facebook_IES_1145_BR_NULL_impressions_per_campaign', 'value': 9121559.0, 'date_time': '2018-06-15', 'ad_platform': 'facebook', 'app_series': 'IES', 'app_id': '1145', 'region': 'BR', 'campaign_tag': 'NULL', 'metric_type': 'impressions_per_campaign', 'anomly_score': 3.616943189065545, 'label': 'True'}, 'sort': [14618]}]
        self.dimension_info = {
                              "took": 1,
                              "timed_out": "false",
                              "_shards": {
                                "total": 5,
                                "successful": 5,
                                "skipped": 0,
                                "failed": 0
                              },
                              "hits": {
                                "total": 108547,
                                "max_score": 0,
                                "hits": []
                              },
                              "aggregations": {
                                "date": {
                                  "doc_count": 4152,
                                  "type": {
                                    "doc_count_error_upper_bound": 0,
                                    "sum_other_doc_count": 0,
                                    "buckets": [
                                      {
                                        "key": "IES",
                                        "doc_count": 2558
                                      },
                                      {
                                        "key": "TopBuzz",
                                        "doc_count": 1594
                                      }
                                    ]
                                  }
                                }
                              }
                            }
        # self.doc = {
        #             "date_time": "2018-06-12",
        #             "ad_platform": "facebook",
        #             "app_series": "IES",
        #             "app_id": "1180",
        #             "region": "US",
        #             "campaign_tag": "NULL"
        #             }
        # self.obj_dict = {
        #                 "install": 10000,
        #                 "CTR": 8.2,
        #                 "CVR": 0.08,
        #                 "ResultRate": 1.2
        #             }
        # self.score_list = [
        #     {
        #         "date_time": "2018-06-12",
        #         "ad_platform": "facebook",
        #         "app_series": "IES",
        #         "app_id": "1180",
        #         "region": "US",
        #         "campaign_tag": "NULL",
        #         "metric_type": "install",
        #         "anomly_score": 0.08,
        #         "label": "True"
        #     }]

    def test_normalization(self):
        """test normalization method."""
        data1 = [1,1,1,1,1,1]
        res = norm_raw(data1)
        self.assertEqual(res, [0,0,0,0,0,0])
        data2 = [1,2,3,4,5]
        res2 = norm_raw(data2)
        self.assertNotEqual(res2, [0,0,0,0,0])
        print('shortDescription(): ', self.shortDescription())

    def test_daterange(self):
        """test daterange."""
        res = daterange(self.FROM_DATE, self.TO_DATE)
        date = ['2018-05-31', '2018-06-01', '2018-06-02', '2018-06-03', '2018-06-04', '2018-06-05', '2018-06-06', '2018-06-07', '2018-06-08', '2018-06-09', '2018-06-10', '2018-06-11', '2018-06-12', '2018-06-13']
        result = [i.strftime("%Y-%m-%d") for i in res]
        self.assertEqual(result, date)
        print('shortDescription(): ', self.shortDescription())

    @mock.patch('elasticsearch.Elasticsearch.search')
    def test_getraw(self, search):
        """test getraw."""
        # Normal value
        search.return_value = self.result_es
        assump = self.result_es['hits']['hits'][0]['_source']['value']
        res = get_raw(self.TO_DATE.strftime("%Y-%m-%d"), self.obj)
        self.assertEqual(assump, res)
        # not found any result
        es2 = self.result_es
        es2['hits']['total'] = 0
        search.return_value = es2
        res2 = get_raw(self.TO_DATE.strftime("%Y-%m-%d"), self.obj)
        self.assertEqual(0, res2)
        # Exception:
        search.return_value = TypeError(" cannot search the result")
        with self.assertRaises(TypeError):
            get_raw(self.TO_DATE.strftime("%Y-%m-%d"), self.obj)
        print('shortDescription(): ', self.shortDescription())

    @mock.patch('elasticsearch.Elasticsearch.search')
    def test_getseries(self, search):
        """test getseries."""
        date = ["2018-06-06"] * 5
        get_series(date, self.obj)
        self.assertEqual(search.call_count, 5)
        search.return_value = self.result_es
        res = get_series(date, self.obj)
        self.assertEqual(res, [95309.0, 95309.0, 95309.0, 95309.0, 95309.0])
        print('shortDescription(): ', self.shortDescription())


    def test_DIFFSTD(self):
        """test DIFFSTD."""
        res1 = DIFFSTD([1],[3])
        self.assertEqual(0, res1)
        res2 = DIFFSTD([1,2,3],[0,0,0])
        self.assertEqual(1.0, res2)
        print('shortDescription(): ', self.shortDescription())

    def test_STREND(self):
        """test DTREND."""
        res1 = STREND([1],[3])
        self.assertEqual(0, res1)
        res2 = STREND([-1,1],[-3,3])
        self.assertEqual(1.0, res2)
        print('shortDescription(): ', self.shortDescription())

    def test_PAA_trans(self):
        """test PAA_trans."""
        res1 = PAA_trans([1,2,3,4,5])
        np.testing.assert_array_equal(np.array([1,2,3,4,5]), res1)
        res2 = PAA_trans([1,2,3,4,5,6,7,8,9,10])
        np.testing.assert_array_equal(np.array([1.5, 3.5, 5.5, 7.5, 9.5]), res2)
        res3 = PAA_trans([1,0,0,1,0,1,0,0])
        np.testing.assert_array_equal(np.array([0.625, 0.125, 0.5 , 0.625, 0.0]), res3)
        print('shortDescription(): ', self.shortDescription())


    def test_SAX_words(self):
        """test SAX_words."""
        res1 = SAX_words([-1,0,1])
        np.testing.assert_array_equal(['a', 'b', 'c'], res1)
        res2 = SAX_words([-1, 0, 1, 1],4)
        np.testing.assert_array_equal(['a', 'c', 'd','d'], res2)
        res3 = SAX_words([-2,-1,-0.2,1,1], 5)
        np.testing.assert_array_equal(['a', 'a','c', 'e', 'e'], res3)
        res4 = SAX_words([-2,-1,0,1,1,1], 6)
        np.testing.assert_array_equal(['a', 'a', 'd', 'f', 'f', 'f'], res4)
        res5 = SAX_words([-2,-1,0,0,1,1,1], 7)
        np.testing.assert_array_equal(['a', 'b', 'd', 'd', 'f', 'f', 'f'], res5)
        with self.assertRaises(Exception):
            SAX_words([1,1,1], 5)
        print('shortDescription(): ', self.shortDescription())


    def test_comb(self):
        """test comb."""
        res1 = comb(3)
        tmp1 = {'aaa': 0,'aab': 0,'aac': 0,'aba': 0,'abb': 0,'abc': 0,'aca': 0,'acb': 0,'acc': 0,'baa': 0,
                 'bab': 0,'bac': 0,'bba': 0,'bbb': 0,'bbc': 0,'bca': 0,'bcb': 0,'bcc': 0,'caa': 0,'cab': 0,
                 'cac': 0,'cba': 0,'cbb': 0,'cbc': 0,'cca': 0,'ccb': 0,'ccc': 0}
        np.testing.assert_equal(tmp1, res1)
        res2 = comb(4)
        np.testing.assert_equal(256, len(list(res2)))
        res3 = comb(5)
        np.testing.assert_equal(3125, len(list(res3)))
        res4 = comb(6)
        np.testing.assert_equal(46656, len(list(res4)))
        res5 = comb(7)
        np.testing.assert_equal(823543, len(list(res5)))
        print('shortDescription(): ', self.shortDescription())


    def test_sax_represent(self):
        """test sax_represent."""
        res1 = SAX_represent(['c', 'c', 'c', 'c', 'c'], comb())
        tmp1 = {'aaa': 0,'aab': 0,'aac': 0,'aba': 0,'abb': 0,'abc': 0,'aca': 0,'acb': 0,'acc': 0,'baa': 0,
                 'bab': 0,'bac': 0,'bba': 0,'bbb': 0,'bbc': 0,'bca': 0,'bcb': 0,'bcc': 0,'caa': 0,'cab': 0,
                 'cac': 0,'cba': 0,'cbb': 0,'cbc': 0,'cca': 0,'ccb': 0,'ccc': 3}
        np.testing.assert_equal(tmp1, res1)
        res2 = SAX_represent(['a', 'b', 'c', 'b', 'a'], comb(4),4)
        self.assertEqual(res2['abcb'],1)
        self.assertEqual(res2['bcba'], 1)
        self.assertEqual(res2['aacb'], 0)
        res3 = SAX_represent(['a', 'b', 'c'], comb(3), 3)
        self.assertEqual(res3['abc'], 1)
        self.assertEqual(res3['aac'], 0)
        with self.assertRaises(Exception):
            SAX_represent(['a', 'b', 'c'], comb(4), 4)
        print('shortDescription(): ', self.shortDescription())


    def test_convert_str(self):
        """test convert_str."""
        tmp = {'aaa': 1,'aab': 0,'aac': 2,'aba': 0,'abb': 0,'abc': 0,'aca': 0,'acb': 0,'acc': 0,'baa': 0,
                 'bab': 0,'bac': 0,'bba': 0,'bbb': 0,'bbc': 0,'bca': 0,'bcb': 0,'bcc': 0,'caa': 0,'cab': 0,
                 'cac': 0,'cba': 0,'cbb': 0,'cbc': 0,'cca': 0,'ccb': 0,'ccc': 3}
        res1 = conver_str(tmp)
        np.testing.assert_equal("102000000000000000000000003", res1)
        print('shortDescription(): ', self.shortDescription())


    def test_is_mindist_zero(self):
        """test is_mindist_zero."""
        res1 = is_mindist_zero("10010", "10011")
        np.testing.assert_equal(1, res1)
        res2 = is_mindist_zero("1001", "10011")
        np.testing.assert_equal(0, res2)
        print('shortDescription(): ', self.shortDescription())


    def test_SAXBAG(self):
        """test SAXBAG."""
        res1 = SAXBAG([0,0,0,0,0],[0,0,0,0,0])
        np.testing.assert_equal(0, res1)
        res2 = SAXBAG([0,0,0,0,0],[1,1,1,1,1])
        np.testing.assert_equal(2, res2)
        print('shortDescription(): ', self.shortDescription())

    def test_normalization(self):
        """test normalization method from anomly detetcion book."""
        res1 = normalization([1,1,1,1,1])
        np.testing.assert_equal([1.0, 1.0, 1.0, 1.0, 1.0], res1)
        res2 = normalization([1,2,1,2,1])
        np.testing.assert_equal([0.8, 0.8, 0.8, 1.6, 1.6], res2)
        print('shortDescription(): ', self.shortDescription())

    def test_zero_scroe_normalization(self):
        """test zero score normalization method."""
        res1 = zero_scroe_normalization([0,0,0,0,0])
        np.testing.assert_equal([0,0,0,0,0], res1)
        res2 = zero_scroe_normalization([1,2,3,2,1])
        np.testing.assert_equal([-1.0, -1.0, 1.0, 1.0, 3.0], res2)
        print('shortDescription(): ', self.shortDescription())


    def test_zero_one_normalization(self):
        """test (0,1) normalization method."""
        res1 = zero_one_normalization([0,0,0,0,0])
        np.testing.assert_equal([0,0,0,0,0], res1)
        res2 = zero_one_normalization([1,2,3,2,1])
        np.testing.assert_equal([0.0, 0.5, 1.0, 0.5, 0.0], res2)
        print('shortDescription(): ', self.shortDescription())


    def test_get_dis_norm(self):
        """test normalization for three distance measurements."""
        # Since the calculation will pass a (0,1) normalization
        # If we calculate a distance for two series, then result would be a list for two distances which are equal to each other,
        # Then if it pass a (0,1) normalization, it will becomes 0.
        # So the minimum of this first parameter's item number should be 3.
        # Otherwise there is no sence to use this method to passing it to a (0,1) normalization.
        res1 = get_dis_norm(STREND, [[2,2,-1,3,1],[1,1,9,1,1],[0,0,0,0,0]])
        np.testing.assert_equal([[0.0, 1.0], [0.0, 1.0], [0, 0]], res1)
        res2 = get_dis_norm(SAXBAG, [[2,2,-1,3,1],[1,1,9,1,1],[0,0,0,0,0]])
        np.testing.assert_equal([[0, 0], [1.0, 0.0], [1.0, 0.0]], res2)
        res3 = get_dis_norm(DIFFSTD, [[2,2,-1,3,1],[1,1,9,1,1],[0,0,0,0,0]])
        np.testing.assert_equal([[1.0, 0.0], [1.0, 0.0], [0.0, 1.0]], res3)
        res_length2 = get_dis_norm(STREND, [[0,-1,0],[1,1,-1]])
        np.testing.assert_equal([[0.5], [0.5]], res_length2)
        with self.assertRaises(Exception):
           get_dis_norm(DIFFSTD, [[0,0,0]])
        print('shortDescription(): ', self.shortDescription())


    def test_average_distance(self):
        """test average distance for neighbourhood calculation."""
        res1 = average_distance([0,0,0,0],[1,1,1,1])
        self.assertEqual(2.0, res1)
        with self.assertRaises(Exception):
            average_distance([0, 0], [1, 1, 1, 1])
        print('shortDescription(): ', self.shortDescription())

    def test_average_distance_normalization(self):
        """test get_averagedis_norm for neighbourhood calculation."""
        res1 = get_averagedis_norm([[0,0,0,0],[1,1,1,1],[2,2,2,2]])
        np.testing.assert_equal([[2.0, 4.0], [2.0, 2.0], [4.0, 2.0]], res1)
        with self.assertRaises(Exception):
            get_averagedis_norm([[0, 0, 0, 0]])
        with self.assertRaises(Exception):
            get_averagedis_norm([[0, 0, 0, 0], []])
        print('shortDescription(): ', self.shortDescription())

    def test_rank(self):
        """test rank."""
        res1 = rank([[10,10,10],[0,0,0],[1,1,1],[2,2,2],[3,3,3]], 1, 5)
        self.assertEqual(3, res1)
        print('shortDescription(): ', self.shortDescription())

    def test_outlinerness(self):
        """test outlinerness."""
        res1 = outlierness([18.0, 2.0, 2.0, 4.0],[[10,10,10,10],[0,0,0,0],[1,1,1,1],[2,2,2,2],[3,3,3,3]])
        self.assertEqual(4.0/3, res1)
        print('shortDescription(): ', self.shortDescription())


    def test_weight(self):
        """test weight."""
        res1 = weight([18.0, 2.0, 2.0, 4.0],[[10,10,10,10],[0,0,0,0],[1,1,1,1],[2,2,2,2],[3,3,3,3]])
        self.assertEqual(3.56, float("{0:.2f}".format(res1)))
        print('shortDescription(): ', self.shortDescription())

    def test_build_dic(self):
        """test build_dic."""
        res1 = build_dic([0,1,2,3,4])
        self.assertEqual([(0, 0), (1, 1), (2, 2), (3, 3), (4, 4)], res1)
        res2 = build_dic([0,0,0,0,0])
        self.assertEqual([(0, 0), (1, 0), (2, 0), (3, 0), (4, 0)], res2)
        print('shortDescription(): ', self.shortDescription())

    def test_avgdis_neigh(self):
        """test avgdis_neigh."""
        # Use a practice data for testing:
        res1 = avgdis_neigh([1.0, 0.67, 1.0, 0.33, 0.0, 0.0, 0.33, 1.0], [(2, 2.27), (1, 2.29), (3, 3.08), (7, 3.13), (4, 3.60), (0, 3.60), (5, 3.64), (6, 3.67)])
        self.assertEqual(0.89, res1)
        print('shortDescription(): ', self.shortDescription())

    def test_dis_to_nei(self):
        """test dis_to_nei."""
        # Use a practice data for testing:
        res1 = dis_to_nei([8.94, 6.71, 4.47, 2.24],[1.0, 1.0, 0.0, 0.0])
        self.assertEqual(0.67, float("{0:.2f}".format(res1)))
        print('shortDescription(): ', self.shortDescription())

    def test_anomly_score(self):
        """test anomly_score."""
        # Use a practice data for testing:
        res1 = anom_score([8.94, 6.71, 4.47, 2.24],[1.0, 1.0, 0.0, 0.0], [0, 0, 0, 0], [0, 0, 0, 0],[[-1,-1,-1,-1,-1],[0,0,0,0,0],[1,1,1,1,1], [2,2,2,2,2], [3,3,3,3,3]] )
        self.assertEqual(2.44, float("{0:.2f}".format(res1)))
        print('shortDescription(): ', self.shortDescription())

    def test_get_anomaly_score(self):
        """test list of a series' anomly_score."""
        # Use a practice data for testing:
        res1 = get_anomaly_score([[1, 1],
                           [1, 1],
                           [1, 1]], [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]],
                          [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]], [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]],
                          [[-1, -1, -1, -1, -1], [0, 0, 0, 0, 0], [1, 1, 1, 1, 1]])
        self.assertEqual(3, len(res1))
        print('shortDescription(): ', self.shortDescription())

    def test_isAbnormal(self):
        """test isAbnormal."""
        res1 = isAbnormal(20,[0,1,2,3,4,5,5,6,20])
        self.assertTrue(res1)
        print('shortDescription(): ', self.shortDescription())

    def test_get_label(self):
        """test get_abnormal."""
        res1 = get_label([0,1,2,3,4,5,5,6,7,20])
        self.assertEqual([False, False, False, False, False, False, False, False, False, True], res1)
        print('shortDescription(): ', self.shortDescription())

    @mock.patch('elasticsearch.Elasticsearch.search')
    def test_queryES(self, mock_es):
        """test queryES"""
        mock_es.return_value = self.result_es
        res1 = queryES(TO_DATE,self.obj)
        self.assertTrue(res1)
        obj = {
                'app_series': "IES",
                'app_id': "1180",
                'region': "US",
                'campaign_tag': "NULL",
                'metric_type': "installs"
            }
        with self.assertRaises(Exception):
            queryES(TO_DATE, obj)
        mock_es.return_value['hits']['total'] = 0
        res2 = queryES(TO_DATE, self.obj)
        self.assertFalse(res2)
        mock_es.return_value = Exception
        with self.assertRaises(Exception):
            queryES(TO_DATE, self.obj)
        print('shortDescription(): ', self.shortDescription())

    @mock.patch('elasticsearch.helpers.scan')
    def test_scanES(self, mock_es):
        """test scanES"""
        mock_es.return_value = [i for i in self.scan_es]
        res1 = scanES(str(TO_DATE), self.obj)
        self.assertEqual(res1, ['onlinegid_byappID', 'impressions_per_campaign'])
        mock_es.return_value = [i for i in []]
        res2 = scanES(str(TO_DATE), self.obj)
        self.assertFalse(res2)
        mock_es.return_value = elasticsearch.exceptions.TransportError(404, "wrong", None)
        with self.assertRaises(Exception):
            scanES(str(TO_DATE), self.obj)
        print('shortDescription(): ', self.shortDescription())

    def test_get_list(self):
        """
        test get_list for 5 dimension details info
        """
        res1 = get_list(self.dimension_info)
        self.assertEqual(["IES", "TopBuzz"], res1)

    @mock.patch('elasticsearch.Elasticsearch.search')
    def test_fetchDate(self, mock_es):
        """test fetchData"""
        mock_es.return_value = self.dimension_info
        fetchData("installs", "2018-05-28")
        mock_es.assert_called_once()
        print('shortDescription(): ', self.shortDescription())

    @mock.patch('elasticsearch.Elasticsearch.search')
    def test_calculate_and_label(self, mock_search):
        """test calculate_and_label"""
        date = ["2018-06-06"] * 14
        mock_search.return_value = self.result_es
        series = get_series(date, self.obj)
        self.assertEqual(series,[95309.0] * 14)
        date = []
        for single_date in daterange(self.FROM_DATE, self.TO_DATE):
            date_tmp = single_date.strftime("%Y-%m-%d")
            date.append(copy(date_tmp))
        self.assertEqual(len(date), len(series))

        subsequence = [series[i:i+self.WINDOW_SIZE] for i in range(len(series) - self.WINDOW_SIZE + 1)]
        strend_norm = get_dis_norm(STREND, subsequence)
        diffstd_norm = get_dis_norm(DIFFSTD, subsequence)
        saxbag_norm = get_dis_norm(SAXBAG, subsequence)

        all_neighbor_distance = get_averagedis_norm(subsequence)
        ascore = get_anomaly_score(all_neighbor_distance, diffstd_norm, strend_norm, saxbag_norm, subsequence)
        abnormal = get_label(ascore)
        score1, label1, value1 = calculate_and_label(self.obj, self.FROM_DATE, self.TO_DATE)
        self.assertEqual(value1, 95309.0)
        self.assertEqual(str(abnormal[-1]), label1)
        self.assertEqual(ascore[-1], score1)

        with self.assertRaises(Exception):
            calculate_and_label(self.obj, datetime.date(2018,5,29), datetime.date(2018,5,31))
        print('shortDescription(): ', self.shortDescription())