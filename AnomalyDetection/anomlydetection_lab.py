
from elasticsearch import Elasticsearch
es = Elasticsearch()

doc = {
    'value': 12345,
    'date_time': '2018-06-07',
    'ad_platform': 'facebook_agency',
    'app_series': 'IES',
    'app_id': '123',
    'region': 'CN',
    'campaign_tag': 'NULL',
    'metric_type': 'installs'
}

update_doc = {
    'script' : 'ctx._source.anomaly_score = '
}

id = doc["date_time"] + "_" + doc["ad_platform"] + "_" + doc["app_series"] + "_" + doc["app_id"] + "_" + doc["region"] + "_" + doc["campaign_tag"] + "_" + doc["metric_type"]
def indexES(doc,id):
    try:
        res = es.index(index="monitor", doc_type='doc', id=id, body=doc)
        print(res['result'])
        return True
    except Exception as err:
        print(err)
        return False


def updateES(doc,id):
    try:
        res = es.update(index="monitor", doc_type='doc', id=id, body=doc)
        print(res['result'])
        return True
    except Exception as err:
        print(err)
        return False

def queryES():
    try:
        es.indices.refresh(index="monitor")
        res = es.search(
            index="monitor",
            body={
                  "query":{
                    "bool":{
                      "must":[{
                        "match": {
                          "ad_platform": "facebook_agency"
                        }
                      },
                      {
                        "match": {
                          "app_id": "123"
                        }
                      },{
                        "match": {
                          "campaign_tag": "NULL"
                        }
                      },{
                        "match": {
                          "region": "CN"
                        }
                      },{
                        "match": {
                          "metric_type": "installs"
                        }
                      },
                      {
                          "match": {
                              "app_series": "IES"
                          }
                      },
                      {
                        "match": {
                          "date_time": "2018-06-07"
                        }
                      }
                      ]
                    }
                  }
                }
        )
        if res['hits']['total'] != 0:
            return True
        else:
            return False
    except Exception as err:
        raise Exception(err)




def getES(id):
    try:
        res = es.get(index="monitor", doc_type='doc', id=id)
        #print("Got %d Hits:" % res['hits']['total'])
        print(res)
        return True
    except Exception as err:
        print(err)
        return False



# Only update the last day:
anomaly_score_field = 'ctx._source.anomaly_score = '
label_field = 'ctx._source.label = \"'
update_doc = {
    'script' : ''
}

id = "2018-06-06" + "_" + "facebook_agency" + "_" + "IES" + "_" + "123" + "_" + "CN" + "_" + "NULL" + "_" + "installs"
def updateES(doc,id):
    try:
        res = es.update(index="monitor", doc_type='doc', id=id, body=doc)
        print(res['result'])
        return True
    except Exception as err:
        print(err)
        return False

# ascore = [10]
# abnormal = [True]
# script = anomaly_score_field + str(ascore[-1]) + "; " + label_field + str(abnormal[-1]) + "\""
# print(script)
# update_doc["script"] += script
# print(update_doc)
# updateES(update_doc, id)
#
print(queryES())


