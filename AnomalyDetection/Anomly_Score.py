from AnormalyDetection import *
import sys
import os
bulk_update_path = os.path.join(os.path.dirname(__file__), "./../BulkElasticsearch/")
sys.path.append(bulk_update_path)
from bulk_update import es_update_bulk
import time
from copy import copy

DEBUG = False
DEBUG_PRINT = True
DEBUG_TO_FILE = True
########################################################################################################################
###################################################### UserCostomize ###################################################
########################################################################################################################

# Setting configuration
config = os.path.join(os.path.dirname(__file__), "config.yaml")
with open(config, "r") as f:
    settings = yaml.load(f)

ES_IP = settings['elasticsearch']['ip']
ES_PORT = settings['elasticsearch']['port']
# instantiate es client, connects to localhost:9200 by default
es = Elasticsearch(ES_IP, port=ES_PORT)
# Setting config for helpers.parallel_bulk:
THREAD_COUNT = settings['anomly_detection']['THREAD_COUNT']
QUEUE_SIZE = settings['anomly_detection']['QUEUE_SIZE']
SERIES_LENGTH = settings['anomly_detection']['SERIES_LENGTH']

#Initialize 5 dimension
PLATFORM = fetchData_monitor('ad_platform', TO_DATE)
APPID = fetchData_monitor('app_id', TO_DATE)
CAMPAIGNTAG = fetchData_monitor('campaign_tag',TO_DATE)
REGIONS = fetchData_monitor('region',TO_DATE)
APPSERIES = fetchData_monitor('app_series',TO_DATE)

########################################################################################################################
####################################################### Main Function ##################################################
########################################################################################################################
def score_by_day(TO_DATE, PLATFORM, APPID, APPSERIES, CAMPAIGNTAG, REGIONS):
    FROM_DATE = TO_DATE - datetime.timedelta(SERIES_LENGTH - 1)
    # Use a flag to trace the process
    i = 1
    time1 = datetime.datetime.utcnow()
    for pla in PLATFORM:
        for ser in APPSERIES:
            for aid in APPID:
                for cam in CAMPAIGNTAG:
                    update_list = []
                    for reg in REGIONS:
                        obj = {
                            "ad_platform": pla,
                            "app_series": ser,
                            "app_id": aid,
                            "campaign_tag": cam,
                            "region": reg,
                        }
                        # Fetch all metric_types for each data:
                        METRIC_TYPE = scanES(str(TO_DATE), obj)
                        if METRIC_TYPE:
                            for typ in METRIC_TYPE:
                                obj["metric_type"] = typ
                                anomly_score, label, value = calculate_and_label(obj, FROM_DATE, TO_DATE)
                                if pla == "facebook" and ser == "IES" and aid == "1145" and cam == "NULL" and reg == "BR" and typ == "CVR":
                                    print("abnormaly score: ", anomly_score, "; label: ", label)
                                if anomly_score != None and label:
                                    obj["date_time"] = str(TO_DATE)
                                    obj["anomly_score"] = anomly_score
                                    obj["label"] = label
                                    if DEBUG_TO_FILE:
                                        line = ""
                                        line += "platform," + pla + ",series," + ser + ",id," + aid + ",campaigntag," + cam + ",region," + reg + ",metric_type," + typ + ",value," + str(
                                            value) + ",score," + str(anomly_score) + ",label," + label
                                        #template = "./../data/SCORE_" + str(TO_DATE) + ".csv"
                                        template = "data/SCORE_" + str(TO_DATE) + ".csv"
                                        with open(template, "a") as file:
                                            file.write(line)
                                            file.write('\n')
                                    # If just simply use append(obj), pointer will just point to the newest obj and the overwrite previous items that "has been append" to update_list
                                    update_list.append(copy(obj))
                                if pla == "facebook" and ser == "IES" and aid == "1145" and cam == "NULL" and reg == "BR" and typ == "CVR":
                                    print("update_list: ", update_list)
                        if DEBUG_PRINT:
                            if int(i / 100) == (i / 100):
                                print("=" * 20, i, " complete.", "=" * 20)
                                localtime = time.asctime(time.localtime(time.time()))
                                print(localtime)
                            i += 1
                    fault_tolerance = 20
                    fault = 0
                    if len(update_list) != 0:
                        res = es_update_bulk(update_list, THREAD_COUNT, QUEUE_SIZE)
                    else:
                        res = True
                    while not res and fault < fault_tolerance:
                        fault += 1
                        res = es_update_bulk(update_list, THREAD_COUNT, QUEUE_SIZE)
                        print(update_list)
                        print(res)
                        print("attempt to update", fault)
    time2 = datetime.datetime.utcnow()
    totalseconds = (time2 - time1).total_seconds()
    print("total seconds costs: ", totalseconds)



# time1 = datetime.datetime.utcnow()
# for single_date in daterange(datetime.date(2018,6,23), datetime.date(2018,6,23)):
#     TO_DATE = single_date
#     print(TO_DATE)
#     # Update 5 necessary variable
#     PLATFORM = fetchData('ad_platform', TO_DATE)
#     APPID = fetchData('app_id', TO_DATE)
#     CAMPAIGNTAG = fetchData('campaign_tag',TO_DATE)
#     REGIONS = fetchData('region',TO_DATE)
#     APPSERIES = fetchData('app_series',TO_DATE)
#
#     if DEBUG:
#         print("=" * 80)
#         print("Platform = ", PLATFORM)
#         print("=" * 80)
#         print("app id = ", APPID)
#         print("=" * 80)
#         print("app series = ", APPSERIES)
#         print("=" * 80)
#         print("campaign tag = ", CAMPAIGNTAG)
#         print("=" * 80)
#         print("region = ", REGIONS)
#
#     score_by_day(TO_DATE,PLATFORM, APPID, APPSERIES, CAMPAIGNTAG, REGIONS)
#     time2 = datetime.datetime.utcnow()
#     totalseconds = (time2 - time1).total_seconds()
#     print("total seconds costs: ", totalseconds)



